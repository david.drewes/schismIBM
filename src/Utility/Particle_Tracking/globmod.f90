!includes now the librarie that calls the global module in the particle
!Tracking

      module globlib
              implicit none 
              public 

              integer,parameter :: sng_kind=4
              integer,parameter :: dbl_kind=selected_real_kind(15,307)

              !Dimensioning parameters
              integer, parameter :: nybte=4
              real(kind=dbl_kind), parameter :: zero=1.e-5
              real(kind=dbl_kind), parameter :: small1=1.e-6
              real(kind=dbl_kind), parameter :: pi=3.1415926d0

              !important variables 
        integer,save ::np,ne,ns,nvrt,mnei,mod_part,ibf,istiff,ivcor,kz,nsig
        real(kind=dbl_kind), save :: h0,rho0,dt
        real(kind=dbl_kind), save :: h_c,theta_b,theta_f,h_s

        !output handles
        character(len=48), save :: start_time,version 
        character(len=12), save :: ifile_char,turbyear_char
        integer,save ::ihot,nrec,nspool,igmp,noutgm,ifile,noutput,ifort12(100)

        integer,save,allocatable :: nne(:),indel(:,:),idry(:),idry_e(:),idry_e0(:)
        integer,save,allocatable :: kbp(:),kbs(:),kbe(:),kbp00(:),isbnd(:)

        real(kind=dbl_kind),save,allocatable ::x(:),y(:),dp(:),eta1(:),eta2(:),eta3(:)
        real(kind=dbl_kind),save,allocatable :: area(:),xctr(:),yctr(:)
        real(kind=dbl_kind),save,allocatable :: snx(:),sny(:),distj(:),dps(:),dldxy(:,:,:)
        
        real(kind=dbl_kind),save,allocatable :: zpar0(:)
        !For interface with util routines 
        real(kind=dbl_kind),save,allocatable :: ztot(:),sigma(:),xcj(:),ycj(:),sigma_lcl(:,:)

        integer,save,allocatable :: i34(:),elnode(:,:),ic3(:,:),elside(:,:),isdel(:,:),isidenode(:,:)
        integer,save,allocatable :: icum1(:,:),icum2(:,:,:)
        integer,save :: nxq(3,4,4),nodel(3),ibmskip,ibmskipmax,mamid
        integer,save,allocatable :: istage(:)

        real(kind=dbl_kind),save,allocatable :: z(:,:)
        real(kind=dbl_kind),save,allocatable ::salt1(:,:),temp1(:,:),salt2(:,:),temp2(:,:)
        real(kind=dbl_kind),save,allocatable::flage1(:,:),diat1(:,:),mizoo1(:,:),mezoo1(:,:)
        real(kind=dbl_kind),save,allocatable::detri1(:,:),nh41(:,:),no31(:,:),po41(:,:),sil1(:,:)
        real(kind=dbl_kind),save,allocatable::flage2(:,:),diat2(:,:),mizoo2(:,:),mezoo2(:,:)
        real(kind=dbl_kind),save,allocatable::detri2(:,:),nh42(:,:),no32(:,:),po42(:,:),sil2(:,:)
        real(kind=dbl_kind),save,allocatable::oxy2(:,:),oxy1(:,:),light2(:),ext2(:,:),ext1(:,:)
        real(kind=dbl_kind),save,allocatable::prey2(:,:),prey1(:,:),dom2(:,:),dom1(:,:)
        real(kind=dbl_kind),save,allocatable :: uu1(:,:),vv1(:,:),ww1(:,:),uu2(:,:),vv2(:,:),ww2(:,:)
        real*8,save,allocatable :: wnx1(:),wnx2(:),wny1(:),wny2(:),hf1(:,:),vf1(:,:),hf2(:,:),vf2(:,:)
        real*8,save,allocatable :: hvis_e(:,:)
        end module globlib
