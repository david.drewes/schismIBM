! This file includes the fortran routines of the Smelt IBM used in
! connection to the SCHISM Particle Tracking 
        !including important files containing different parameters
        !needed in the Smelt IBM 
!       include 'c_track.f'
!        include 'c_smelt.f'
        module ibm 
        implicit none
        public 

        !declaring of various parameters used in the IBM 
        real,save :: tspwn !spawning Temperature [°C]
        integer :: spwnlimday=10 ! days till spawn
        integer,save :: iturb
        real(8),save,allocatable ::EggDev1(:),EggDev2(:),EggMort(:)
        real*8,save,allocatable :: YOlkDev1(:),YolkDev2(:)
        integer,save,allocatable :: limdays(:),indperPart(:)
        contains 
        
      subroutine smelt_initialising(nparticle,is_start,EggHatchLength,Lengthpar1,Lengthpar2,Weightpar1,Weightpar2)
        use globlib
        !declaring the variables
        integer,intent(in) :: nparticle
        real*8,intent(out),allocatable ::EggHatchLength(:),Weightpar1(:),Weightpar2(:)
        real*8 :: len_init
        integer,intent(out) :: is_start
        real*8,intent(out),allocatable :: Lengthpar1(:),Lengthpar2(:)
        integer :: istat,Spawntime,prmin,prmax,pincr
        !real*8,intent(out) :: T_limlow
        character(len=8) :: test
        !reading in values from c_smelt.f90
        open(199,file='smelt_param.txt',action='read')
        read(199,*) 
        read(199,*) istat
        !print*,'istat=',istat 
        read(199,*) tspwn
        read(199,*) len_init
        read(199,*) iturb
        !print*,'T_limlow=',T_limlow
        !initialising the values 
        is_start = istat
        print*,'is_start',is_start
        allocate(istage(nparticle),EggDev1(nparticle),&
        &EggMort(nparticle),EggDev2(nparticle), &
        &indperPart(nparticle),EggHatchLength(nparticle))
        allocate(Lengthpar1(nparticle),Lengthpar2(nparticle))
        allocate(YolkDev1(nparticle),YolkDev2(nparticle),Weightpar1(nparticle),Weightpar2(nparticle))
        if (istat==1) then 
           istage(1:nparticle) = 0
           EggHatchLength(1:nparticle) = 0
           Weightpar1(1:nparticle) = 0.0
           Weightpar2(1:nparticle) = 0.0
           Lengthpar1(1:nparticle) = 0.0
           Lengthpar2(1:nparticle) = 0.0
        elseif(istat.ne.1) then 
           istage(1:nparticle) = 0
           print*,'len_init',len_init
           EggHatchLength(1:nparticle) = len_init
           Weightpar2(1:nparticle) = 0.0033*(len_init*0.1)**3.4330
           Weightpar1(1:nparticle) = 0.0
           Lengthpar2(1:nparticle) = len_init
           lengthpar1(1:nparticle) = 0.0
           print*,Lengthpar1,Lengthpar2
        endif 
        indperPart(1:nparticle) =1000000; !Number of Individuals per Particle
        EggDev1(1:nparticle) = 0.0
        EggDev2(1:nparticle) = 0.0
        YolkDev1(1:nparticle)=0.0
        YolkDev2(1:nparticle) = 0.0
        EggMort(1:nparticle) = 0.0
        return
      end subroutine smelt_initialising
        

!        subroutine TS_read_ibm(i,nparticle,ie4,levind,Ttpar,Sspar)
!                use netcdf
!                use globlib 

                !declaration of used variables 
!                character(len=30) :: file63
!                integer :: j,levind,ie4,isalt,itemp,iret,i,nparticle
!                integer :: ind,irec1,ncid,len_char
!                real,allocatable ::real_ar(:,:)
!                real*8,intent(out) :: Ttpar,Sspar
!                real*8,allocatable :: temp_face(:),salt_face(:) 
                !determining the needed nc_input file 
!                write(ifile_char,'(i12)')ifile 
!                ifile_char=adjustl(ifile_char);len_char=len_trim(ifile_char)
!               file63='schout_'//ifile_char(1:len_char)//'.nc'

                !opening the file to retrieve temperature and salinity 
!                iret=nf90_open(trim(adjustl(file63)),OR(NF90_NETCDF4,NF90_NOWRITE),ncid)
!                iret = nf90_inq_varid(ncid,'temp',itemp)
!                if (iret/=nf90_NoErr) then 
!                        stop 'temp not found'
!                endif 
!                iret = nf90_inq_varid(ncid,'salt',isalt)
!                if(iret/=nf90_NoErr) stop 'salt not found'

                !allocation of the arrays introduced before 
!                allocate(real_ar(nvrt,np),temp(np,nvrt),salt(np,nvrt),temp_face(3),salt_face(3))

!                if(ibf==1) then 
!                        irec1=1
!                else 
!                        irec1=nrec
!                endif

                !retrieving the values for T and S
!                iret=nf90_get_var(ncid,itemp,real_ar(1:nvrt,1:np),(/1,1,irec1/),(/nvrt,np,1/))
!                temp(:,:) = transpose(real_ar(1:nvrt,1:np))
!                iret=nf90_get_var(ncid,isalt,real_ar(1:nvrt,1:np),(/1,1,irec1/),(/nvrt,np,1/))
!                salt(:,:) = transpose(real_ar(1:nvrt,1:np))
                !averaging the values over the three nodes 
!                temp_face(1:3) = 0.0
!                salt_face(1:3) = 0.0

!                if(levind>0 .and. levind<=21) then 
!                      do j=1,3
!                        ind = elnode(j,ie4)
!                        temp_face(j) = temp(ind,levind)
!                        salt_face(j) = salt(ind,levind)
!                      enddo
                       !averaging over three nodes to retrieve a
!                       !particle value 
!                     tpar(i) = sum(temp_face(1:3))/3.
!                     spar(i) = sum(salt_face(1:3))/3.

!                endif

!                      return
!                end subroutine TS_read_ibm

      subroutine smelt_egg(i,nparticle,n_start,temp,salt,EggDev1,EggDev2,N_individuals,EggHatchLength)
           use globlib 
        integer,intent(in) :: nparticle,i
        real*8,intent(in) :: temp,salt,n_start
        real*8 :: y1,y2,a,b,T_limlow,egg_mort,egg_surv,rePar
        real*8,intent(inout) ::EggDev1(:),EggDev2(:),EggHatchLength
        real*8,intent(inout) :: N_individuals(:)
        real*8 :: tstep_ibm,dec
        !parameters due to Lillelund
        if(istage(i)==0) then 
          if(temp.gt. tspwn) then 
                  istage(i)=1
                  print*,'istage(i)=1',i,temp,tspwn
          endif 
        endif
        a=624.0
        b=-0.5994
        dec = 0.06/(86400./dt)
        !print*,'dec',dec,dt,86400/dt
        tstep_ibm = 86400./dt
        !print*,'T_limlow_egg',tspwn 
        !calculating the daily development 
        !checking if tpar is above Tlimit
        if(temp.gt.tspwn .and. istage(i)==1) then
           y2=(a*temp**(b))*tstep_ibm
           y1 = 1./(y2/temp)
           !print*,'TPAR(i)',tpar(i)
           if (EggDev2(i).lt.1.0) then 
              EggDev1(i) = EggDev2(i) + y1
              print*,'EggDev1bm',EggDev1(i),y1,EggDev2(i)
              if (EggDev1(i)<1.0) then 
                 !calculate mortality
                 rePar = N_individuals(i)
                 egg_mort= dec*real(N_individuals(i))
                 rePar = rePar - egg_mort
                 N_individuals(i) = rePar
              else if(EggDev1(i).gt.1.0) then 
                 istage(i) = 2
              endif    
           endif
           else if(temp .lt. tspwn .and. istage(i)==1) then 
              EggDev1(i) = EggDev2(i)
              rePar = N_individuals(i)
              dec = 0.2/(86400./dt)
              egg_mort = dec*real(N_individuals(i))
              rePar = rePar - egg_mort
              N_individuals(i)=rePar
        endif 
        return
      end subroutine smelt_egg

      subroutine smelt_yolksac(i,nparticle,temp,YolkDev1,YolkDev2,N_individuals,lengthinpar,weightinpar,factlim)
      use globlib
      integer,intent(in) :: i,nparticle
      real*8,intent(in) :: temp
      real*8,intent(out) :: factlim
      real*8,intent(inout) :: N_individuals(nparticle)
      real*8,intent(inout):: YolkDev1(:),YolkDev2(:)
      real*8,intent(inout):: lengthinpar,weightinpar
      real*8 :: tstep,y2,dec,rePar,Yolk_mort
      real*8 :: ltes

      !calculating the development of the YOlk-sac phase
      ! not finished by now as literature based formulas are still
      ! missing 
      ! using Utes Sprat formula for the time being 
      tstep = 86400./dt
      dec = 0.03/tstep
      if(temp.gt.tspwn) then
        y2 = 0.42*temp**(1.495)*tstep
                
        if(YolkDev2(i).lt.1.0) then 
          YolkDev1(i) = YolkDev2(i) + 1./y2
          rePar = N_individuals(i)
          Yolk_mort = dec*real(N_individuals(i))
          rePar = rePar-Yolk_mort
          N_individuals(i) = rePar
          if(YolkDev1(i).gt.1.0) then 
            istage(i) = 3
            lengthinpar = 7.0 !quick and dirty solution
            weightinpar = 0.0033*(lengthinpar*0.1)**3.4330  
            ltes = 1000.*(weightinpar/(lengthinpar*0.1)**3)
            factlim = 1./ltes
            !print*,'Larvae is now exogenously feeding'
          endif 
        endif
      endif
      return
      end subroutine smelt_yolksac

      subroutine smelt_larvae(i,iitrb,maxnpar,tpar,tbpar,spar,expar,oxpar,leng1,leng2,wegin1,wegin2,swimspd,lfac,metab,factlim,fpr,plx,wpr,prey,iipr)
      use globlib
      integer,intent(in) :: maxnpar,i,iipr,iitrb
      real*8,intent(inout)::tpar,spar,oxpar,expar,wegin1,wegin2
      real*8,intent(inout) :: leng1,leng2,tbpar
      real*8,intent(in) :: fpr(iipr),wpr(iipr),plx(iipr),prey
      real*8,intent(out)::lfac,swimspd,metab
      real*8,intent(in)::factlim
      real*8:: mr,mrr,sda,ma,dt_larvae,weight(maxnpar)
      real*8 :: a17,b17,mr17,k,ae,cgr,Cmax,prcon(maxnpar)
      real*8 :: Growth,prcons(maxnpar),wla_old
      real*8 :: plmax,CSmax,yvar,CS(iipr),partpr(iipr)
      real*8 :: partpr1(iipr),pbiomax,TT,C,CC,ivecii(iipr)
      real*8 :: rank(iipr),rankk
      integer ::irind(iipr),irind1(iipr)
      real*8 :: ss,alp,HT(iipr),dpr(iipr),rd(iipr),ssp
      real*8 :: dken,xte,turbs,psp1,ttt
      real*8 :: ecs,ra(iipr),epr(iipr),psp(iipr),xpr(iipr),xprsum
      real*8 :: summe,summe1,summe2,summealt,summe2alt
      real*8 :: partpr2(iipr),wpr2(iipr)
      real*8 :: p4hr(maxnpar,5),sum,ger
      real*8 :: dweight,dweightm,dweightp
      real*8 :: weightm,weightp,lengthnew
      real*8 :: lengthparin
      real*8 :: FT,PT,oxy_dec
      real*8 :: dw_old,weight_new,weightdiff
      integer :: ipr,jj,iy
         open(299,file='larvae_debug.txt',access='append')
      a17 = 1.23
      b17 = 0.72
      lengthnew = 0.0 
      weight_new = 0.0     
      dt_larvae = 86400./(dt*12.)
      write(299,*) 'particle: ',i
      write(299,*) 'length larvae start=',leng1
      !calculate the wws from the length
      ! length treated as fork length [cm] 
      ! weight in [g] 
      
      weight(i) = wegin2
      lengthparin = leng2
      print*,'weight(i)',weight(i)
      print*,'lenghtparin start',lengthparin
      write(299,*) 'weight begin=',wegin2,leng2
      ! weight conversion 
      dweight = weight(i)*0.091 ! dry weight [g]
      weightm = weight(i)*10**3 ! wet weight [mg]
      dweightm = dweight*10**3 ! dry weight [mg]
      weightp = weight(i)*10**6 ! wet weight [mikrogram]
      dweightp = dweight*10**6 ! dry weight [mikrogram]
      write(299,*) 'dweight begin=',dweightp

      !metabolism (Standart) 
      mrr=(((((0.0019*weight(i)**(-0.1057)*exp(0.0730*tpar))/1.429)*10.**6)/24.)*weight(i))
      mr = mrr*0.00463*227.0 !conversion of units to [piko l ]
      write(299,*) 'mr',mr,tpar
      !foraging during the daytime
      write(299,*) 'extpar',expar
        
      if(expar.gt.0.017) then 
        write(299,*) 'day means foraging'
        ss = 181.15/(1+exp(-(lengthparin-29.52)/5.52))
        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        ! TODO: Oxygen parameterisation needs to be added! 
        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        if(oxpar.lt.150) then 
          oxy_dec = 0.0132*oxpar-0.99
          ss = ss*oxy_dec

          if(ss.lt.0.0) ss =0
        endif
        swimspd = ss  
        alp=0.01667*(1.2500+2657/lengthparin)
        alp = alp*pi/180.0
        !print*,'alp',alp
        do ipr=1,iipr
          HT(ipr)=exp(0.264*10**(7.0151*(plx(ipr)/lengthparin)))
          dpr(ipr)=fpr(ipr)*prey
          rd(ipr) = lengthparin/(2.0*tan(alp/2.0))
          write(299,*) 'rd before',rd(ipr)
          !impact of turbidity
          if(iitrb==1) then 
          if(tbpar.le.200.0) then 
            PT=0.5+0.0025*tbpar
            write(299,*) PT,'case 1'
          elseif(tbpar.gt.200.0 .and. tbpar.le.300.0) then 
            PT=1.3-0.0015*tbpar
            write(299,*) PT,'case 2'
          elseif(tbpar.gt.300.0) then
            PT=1.45-0.002*tbpar
            write(299,*) PT,'case 3'
          endif
          rd(ipr)=rd(ipr)*PT
          elseif(iitrb==0) then 
            PT = 1.0
          endif
          write(299,*) 'rd(ipr) after',rd(ipr)
                
          ssp =3.*plx(ipr)

          if(iturb==0) then                  
             ecs = sqrt(ssp**2+ss**2)
             psp(ipr) = 1.
          elseif(iturb==1) then 
             if(dken==0) then 
                psp(ipr)=1.
                ecs = sqrt(ssp**2.+ss**2.)
             else
                xte = 3.615*(dken*rd(ipr))**0.667
                if(xte.lt.0.) then 
                   psp(ipr)=1.
                elseif(xte.ge.0.) then
                   turbs = sqrt(xte)
                   ecs = sqrt(ssp**2.+ss**2.+turbs**2.)
                   call prosp(rd(ipr),turbs,HT(ipr),psp1)
                   ttt=turbs*HT(ipr)
                   if(ttt.lt.rd(ipr)) then 
                      psp(ipr)=1./rd(ipr)*(rd(ipr)-ttt+psp1)
                   elseif(ttt>=rd(ipr) .and. ttt<(2.*rd(ipr))) then 
                      psp(ipr) = 1./rd(ipr)*((rd(ipr)**3.)/(ttt**3.)*(ttt-rd(ipr))+psp1)
                   elseif(ttt>(2.*rd(ipr))) then 
                      psp(ipr)=rd(ipr)**3./(ttt**3.)
                   endif 
                endif
             endif 
          endif    
          ra(ipr) = pi/2.*rd(ipr)**2.
          epr(ipr) = ecs*ra(ipr)*dpr(ipr)
          xpr(ipr)=epr(ipr)
        enddo

        !consumption
        plmax = 1524.9/(1.0+(lengthparin/14.0)**(-1.63))
        write(299,*),'plmax',plmax,prey
        write(299,*), (1.0+(lengthparin/14.0)**(-1.63))
        CSmax = 1.1
        yvar = (CSmax*lengthparin)/(plmax*10.**(-3))
        print*,'yvar',yvar
        print*,'lenghtin',lengthparin
        do ipr=1,iipr
           CS(ipr) = CSmax-(yvar*(plx(ipr)/lengthparin))
           if(CS(iipr).lt.0.0) then 
              CS(ipr) = 0.0
           endif 
           wpr2(ipr)=2.*wpr(ipr)
           rank(ipr) = (wpr2(ipr)*CS(ipr))/HT(ipr)
        enddo 
        print*,'CS',CS(:)
        print*,'plx',plx(:)
        print*,'wpr',wpr2(:)
        print*,'HT',HT(:)
        print*,'rank',rank(:)
        !print*,'------------------------'
        !print*,'CS(ipr)'
        !print*,CS(:)
        !print*,'------------------------'
        do ipr=1,iipr
           irind1(ipr) = 1
           irind(ipr) = 999.
        enddo 
        do ipr=1,iipr-1
          iy = 0
          rankk = rank(ipr)
          do jj=ipr+1,iipr
             if(rankk<rank(jj)) then
               iy=iy+1
             else
               irind1(jj) = irind1(jj)+1
             endif 
          enddo
          irind1(ipr) = irind1(ipr)+iy
        enddo
        print*,'irind1',irind1
        do jj = 1,iipr
           if(rank(jj).gt.0) then 
             irind(irind1(jj))=jj
           endif
        enddo
        print*,'irind',irind
        summe = 0.0
        summe1 = 0.0
        summe2 = 0.0

        do jj = 1,iipr
           if(irind(jj).ne.999.) then 
              summealt=summe
              summe2alt=summe2
              write(299,*) irind(jj),'irind(ipr)',jj
              write(299,*)xpr(:),'xpr',xpr(irind(jj))
              partpr(irind(jj))=xpr(irind(jj))*wpr2(irind(jj))*CS(irind(jj))*psp(irind(jj))
              print*,'partpr(int(irind(ipr)))',partpr(irind(jj))
              write(299,*)xpr(irind(jj)),'xpr'
              write(299,*) ,wpr2(irind(jj)),'wpr'
              write(299,*) CS(irind(jj)),'CS'
              write(299,*) psp(irind(jj)),'psp'
              summe1=summe1+partpr(irind(jj))
              summe2 = summe2+xpr(irind(jj))*HT(irind(jj))
              summe = summe1/(1.+summe2)

              if(summe.lt.summealt) then 
                summe = summealt
                summe2=summe2alt
              endif 
              ivecii(irind(jj))=1
           endif
        enddo 
        C=summe*dt
        print*,'C',C,summe
        write(299,*) 'C ',C,'summe ',summe,'dt ',dt
        ! calculation of cmax
        if(tpar>3 .and. tpar<16) then 
          FT = 0.0466*tpar+0.2662
        elseif(tpar>=16 .and. tpar<21) then 
          FT = 0.98
        elseif(tpar>21) then 
          FT = -0.194*tpar+5.045
        endif 

        Cmax =((((((0.0290*weight(i)**(-0.2750))*FT)*PT))*(10.**6))/24.)*weight(i)
        print*,'Cmax',Cmax
        if(C>Cmax) then 
          C=Cmax
          write(299,*) 'C=Cmax',Cmax,'FT',FT,ipr,'ipr','PT',PT
        endif 
        
        k = 2.5
        !!!! Oxygen fehlt hier noch !!!!
        if(oxpar .gt. 150.0) then
          ma = k*mr
        else
          ma = (k+(1-oxy_dec))*(mr*(2-oxy_dec))
        endif
        write(299,*) 'ma, ',ma,'k ',k
        ae = 0.7*(1.0-0.3*exp(-0.003*(dweightp-196.)))

        write(299,*) 'ae*C, ',ae*c,'ae',ae
        if (ae*C.gt.ma) then 
           sda = 0.19
        else
           sda = 0.0
        endif 

        cgr = C*ae*(1.-sda)
        write(299,*) 'cgr, ',cgr

        Growth = cgr-ma
        write(299,*) 'Growth, ',Growth
        elseif(expar.lt. 0.017) then 
          C = 0.0
          ae=0.7*(1.-0.4*exp(-0.003*(dweightp-196.)))
          if (ae*C .gt.ma) then 
             sda = 0.19
          else
             sda = 0.0
          endif 
          cgr = C*ae*(1.0-sda)
          Growth = cgr-mr

        endif !day and night 
       ! growth calculation
        dw_old = dweightp
        dweightp = dweightp+Growth
        !weight_new = (dweightp/0.091)*10**-6 !wet weight [g]
        weight_new = (dweightp/0.091)
        weight_new = weight_new*(10.**(-6))
        wegin1 = weight_new
        weightdiff = weight_new-weight(i)
        write(299,*)'weightdiff',i,weightdiff,weight_new,weight(i),dw_old,dweightp
        
       if(wegin1.gt.2*weight(i)) then 
         print*,'unrealisitc growth',weight(i),2*wla_old
       endif 
       lengthnew=((10.*10.**(567./3433.)*weight_new**(1000./3433.))/33.**(1000./3433.))*10. 
       write(299,*) 'length end',lengthnew, lengthparin 
       !print*,lengthparin,length_old
       if (lengthnew .lt.lengthparin) then 
          lengthnew = lengthparin
       endif

       !calculating the life quality factor 
       lfac = factlim*(1000.*(weight_new/(lengthnew*0.1)**3.0))
       write(299,*) 'lfac',lfac,factlim,lfac-factlim
       metab = mr
       leng1 = lengthnew
       write(299,*) 'lengthparin end', lengthparin
       ! print*,'ipr',', dpr',', rd',', HT'
        
        write(299,*),'----------------------------------------------'
        close(299)

      print*,'larvae is calculated'
      !next calucations are only done during the day (ext>0.01)
      return
      end subroutine smelt_larvae
      
      subroutine ibm_mortality(istge,npar,temp,oxy,weight,lfac,light)
      use globlib
      real*8,intent(in):: temp,oxy,lfac,light,weight
      integer,intent(in) :: istge
      real*8,intent(inout)::npar
      real*8::mort_egg,mort_yolk,mort_larv
      real*8:: surv_egg,surv_yolk,Ni

      !calculation of the mortality rates depending on the life stage of
      !the individual
      Ni = npar
      !if(istge ==1) then 
      !   mort_egg = 0.006/(86400./3600.)
      !   Ni = Ni-(Ni*mort_egg)
      !elseif(istge ==2) then 
      !   if(oxy.gt.150) then 
      !      mort_yolk = 0.00003/(3600./86400.)
      !   elseif(oxy.le.150) then 
      !      mort_yolk = 0.00009/(3600./86400.)
      !   endif 
      !   Ni = Ni-(Ni*mort_yolk)
      if(istge ==3) then 
         if(lfac.lt.0.6 .and. light.gt.0.017) then 
           mort_larv = (10.3*10.**(-3.)*(weight*10.0**3.)**(-0.5))/86400.
         else
           print*,'is calucalted'       
           mort_larv = (5.3*10.**(-3.)*(weight*10.0**3.)**(-0.25))/86400.
         endif
         print*,'larvae mort before:',Ni,mort_larv,lfac
         Ni = Ni*(exp(-mort_larv*3600.))
      endif
      !updating npar 
      print*,'npar after',Ni
      npar = Ni
      return 
      end subroutine ibm_mortality

      subroutine create_netcdf_output_ibm(maxnpar)
        use globlib
        use netcdf
        integer :: ncidi,ireti,x_dim
        integer :: x_dimid,t_dimid,idimids(2)
        integer,intent(in) :: maxnpar
        integer :: dim_maxnpar(maxnpar)
        integer :: ivarid1,ivarid2,ivarid3,ivarid4,ivarid5
        integer :: ivarid6,ivarid7,ivarid8,ivarid9,ivarid10
        x_dim = maxnpar
        dim_maxnpar=(/x_dim/)
        ireti = nf90_create('egg_output.nc',NF90_CLOBBER,ncidi)
        call check(nf90_def_dim(ncidi,'npar',x_dim,x_dimid))
        call check(nf90_def_dim(ncidi,'time',nf90_unlimited,t_dimid))
        idimids = (/x_dimid,t_dimid/)
        call check(nf90_def_var(ncidi,'time',NF90_REAL,t_dimid,ivarid1))
        call check(nf90_def_var(ncidi,'npar',NF90_INT,idimids,ivarid2))
        call check(nf90_def_var(ncidi,'EggDev',NF90_REAL,idimids,ivarid3))
        call check(nf90_def_var(ncidi,'EggHL',NF90_REAL,idimids,ivarid4))
        call check(nf90_def_var(ncidi,'YolkDev',NF90_REAL,idimids,ivarid5))
        call check(nf90_def_var(ncidi,'LarvaeLength',NF90_REAL,idimids,ivarid6))       
        call check(nf90_def_var(ncidi,'LarvaeWeight',NF90_REAL,idimids,ivarid7))
        call check(nf90_def_var(ncidi,'RetenSpeed',NF90_REAL,idimids,ivarid8))
        call check(nf90_def_var(ncidi,'lifeq',NF90_REAL,idimids,ivarid9))
        call check(nf90_def_var(ncidi,'metab',NF90_REAL,idimids,ivarid10))
        call check(nf90_enddef(ncidi))
        call check(nf90_put_var(ncidi,ivarid2,(/x_dim/)))
        call check(nf90_close(ncidi))
      end subroutine create_netcdf_output_ibm

      subroutine write_netcdf_output_ibm(time,maxnpar,itot,iit,EggDev,EggHatchLength,YolkDev,WeightLarvae,&
        & LengLarvae,Rspeed,lifeeq,metabb,iihot,ihot_start)
        use globlib 
        use netcdf
        implicit none
        character(len=50) :: outfile
        integer,intent(in) :: maxnpar,iihot,ihot_start
        real*8,intent(in) :: YolkDev(maxnpar),Rspeed(maxnpar)
        real*8,intent(in) ::EggDev(maxnpar),EggHatchLength(maxnpar)
        real*8,intent(in) :: LengLarvae(maxnpar),WeightLarvae(maxnpar)
        real*8,intent(in) :: lifeeq(maxnpar),metabb(maxnpar)
        integer :: x_dim,t_dim,fileID
        real*8,intent(in) :: time
        integer,intent(in) :: itot,iit
        integer::x_dimid,t_dimid,idiff
        integer :: start_1d(1),count_1d(1)
        integer,dimension(2) :: dimids,start,count
        integer::t_varid,x_varid,varid1,varid2,varid3,varid4
        integer :: vTime,vI,vD,vL,vY,dday(2),vLL,vLW,vRS
        integer :: vLQ,vMR
        real*8 :: atime(1)
        idiff = iit+1-itot
        if(mod(idiff,1)==0 .or. idiff==1) then 
        call check(nf90_open(path='./egg_output.nc',mode=nf90_write,ncid=fileID))
        call check(nf90_inq_varid(fileID,'time',vTime))
        call check(nf90_inq_varid(fileID,'EggDev',vI))
        call check(nf90_inq_varid(fileID,'EggHL',vL))
        call check(nf90_inq_varid(fileID,'YolkDev',vY))
        call check(nf90_inq_varid(fileID,'LarvaeLength',vLL))
        call check(nf90_inq_varid(fileID,'LarvaeWeight',vLW))
        call check(nf90_inq_varid(fileID,'RetenSpeed',vRS))
        call check(nf90_inq_varid(fileID,'lifeq',vLQ))
        call check(nf90_inq_varid(fileID,'metab',vMR))
        if(idiff==1) then
          if(iihot==0) then  
           dday(1)=idiff
          elseif(iihot==1) then 
           dday(1)=idiff+ihot_start
          endif 
        else
           dday(1) = dday(1)+1
        endif 
        start(2) = dday(1);start(1)=1
        count(1) = maxnpar;count(2) = 1
        start_1d(1) = dday(1);count_1d(1)=1
        atime(1) = time
        call check(nf90_put_var(fileID,vTime,atime,start_1d,count_1d))
        call check(nf90_put_var(fileID,vI,EggDev(:),start,count))
        call check(nf90_put_var(fileID,vL,EggHatchLength(:),start,count))
        call check(nf90_put_var(fileID,vY,YolkDev(:),start,count))
        call check(nf90_put_var(fileID,vLL,LengLarvae(:),start,count))
        call check(nf90_put_var(fileID,vLW,WeightLarvae(:),start,count))
        call check(nf90_put_var(fileID,vRS,Rspeed(:),start,count))
        call check(nf90_put_var(fileID,vLQ,lifeeq(:),start,count))
        call check(nf90_put_var(fileID,vMR,metabb(:),start,count))
        call check(nf90_close(fileID))
        endif
        return
        end subroutine write_netcdf_output_ibm
        
        subroutine check(istatus)
        use netcdf 
        implicit none 
        integer,intent(in) :: istatus 
        if (istatus/=nf90_noerr) then 
                write(*,*) trim(adjustl(nf90_strerror(istatus)))
                stop
        end if 
        end subroutine check

        subroutine smelt_hotstart(nparticles,Dev_hot,Day_hot,HL_hot,weipar,lengpar,YoHot,stghot)
        use globlib
        use netcdf
        implicit none
        integer,intent(in):: nparticles
        real*8,intent(inout) :: Dev_hot(nparticles),Day_hot(nparticles),HL_hot(nparticles)
        real*8,intent(inout) ::weipar(nparticles),lengpar(nparticles),YoHot(nparticles)
        integer,intent(inout) :: stghot(nparticles)
        integer :: irhot,nchot,i1,i2,i3,i4,i5,i6,ishot(nparticles)
        real*8::real_er(nparticles),e1(nparticles),e2(nparticles),e3(nparticles)
        real*8 :: e4(nparticles),e5(nparticles),e6(nparticles)
        irhot=nf90_open('./hotstart_ptrack.nc',mode=nf90_nowrite,ncid=nchot)
        if(irhot/=nf90_noerr) stop 'could not open egg hotstart'


        print*,'eggdev'
        call check(nf90_inq_varid(nchot,'EggDev',i1))
        print*,'Weight'
        call check(nf90_inq_varid(nchot,'pstage',i2))
        print*,'pstage'
        call check(nf90_inq_varid(nchot,'weight',i4))
        print*,'inq egg hot done'
        call check(nf90_inq_varid(nchot,'length',i5))
        print*,'inq length'
        call check(nf90_inq_varid(nchot,'YolkDev',i6))
        print*,'inq YolkDev'
        !reading the variables
        print*,'egg dev read'
        call check(nf90_get_var(nchot,i1,real_er(1:nparticles)))
        e1(1:nparticles) = real_er
        print*,'Weight read'
        call check(nf90_get_var(nchot,i2,real_er(1:nparticles)))
        ishot(1:nparticles) = int(real_er(1:nparticles))
        print*,'pstage read'
        call check(nf90_get_var(nchot,i5,real_er(1:nparticles)))
        e5(1:nparticles) = real_er
        print*,'Length read'
        call check(nf90_get_var(nchot,i6,real_er(1:nparticles)))
        e6(1:nparticles) = real_er
        print*,' Yolk Dev read'
        call check(nf90_get_var(nchot,i4,real_er(1:nparticles)))
        e4(1:nparticles) = real_er(:)
        print*,'read done'
        Dev_hot(1:nparticles) = e1(:)
        print*,'DevHot'
        weipar(:) = e4(:)
        lengpar(:) = e5(:)
        YoHot(:) = e6(:)
        stghot(:) = ishot(:)

        return
        end subroutine smelt_hotstart

        subroutine lightext(lightin,flag,diat,ddom,ddetri,dspth,btm_ind,extt)
        !calculates the shading of the radiation due to plankton
        use globlib
        implicit none
        real*8,intent(in):: lightin(np),flag(np,nvrt),diat(np,nvrt)
        real*8,intent(in) :: dspth(np,nvrt),ddom(np,nvrt),ddetri(np,nvrt)
        integer,intent(in) :: btm_ind(np)
        real*8,intent(out) :: extt(np,nvrt)
        real*8 :: BioC(2),EXtot1,diatt,flagg
        real*8 :: domm,dett,tpp
        real*8,allocatable :: ExDetri(:,:),dpd(:,:)
        real*8,allocatable :: EXwater(:,:),EXphyto(:,:)
        integer :: i,j,iind
        print*,'light ext start'
        BioC(1) = 0.05 !light extinction [1/m]
        BioC(2) = 0.03/(6.625*12.01) ! phyto selfshading [m**2/(mgC)]
        print*,'light ext before allocate'
        allocate(ExDetri(np,nvrt),dpd(np,nvrt),EXwater(np,nvrt),EXphyto(np,nvrt))
        extt(1:np,1:nvrt)=0.0
        dpd(:,:)=0.0
        ExDetri(:,:) = 0.0
        EXwater(:,:) = 0.0
        do i=1,np
          do j=1,nvrt
            EXphyto(i,j) = 0.0
            extt(i,21) = lightin(i) 
          enddo 
        enddo
        print*,'allocate light ext done'
        print*,'btm_ind()',btm_ind(39567),nvrt
        print*,'-------------------'
        print*,'z()',dspth(39567,:)
        !calculate the layer thickness 
        do i=1,np
           iind = btm_ind(i)+1
           if(iind==0) then
           iind=1
           endif 
           do j=nvrt,iind,-1
              tpp = dspth(i,j)-dspth(i,j-1)
              dpd(i,j) = tpp
           enddo
        enddo
        print*,'-------------'
        print*,'dpd()',dpd(39567,:)
        do i=1,np
           iind = btm_ind(i)-1
           if(iind==0) then 
             iind=1
           endif
          do j=nvrt-1,iind,-1
            EXwater(i,j)=EXwater(i,j+1)+BioC(1)*dpd(i,j+1)
            flagg = flag(i,j+1)
            diatt = diat(i,j+1)
            domm = ddom(i,j+1)
            dett = ddetri(i,j+1)
            EXphyto(i,j) = EXphyto(i,j+1)+(flagg+diatt)*dpd(i,j+1)
            EXdetri(i,j) = EXdetri(i,j+1)+(dett+domm)*dpd(i,j+1)
            EXtot1 = BioC(2)*EXphyto(i,j)+EXwater(i,j)+BioC(2)*Exdetri(i,j)
            extt(i,j) = lightin(i)*exp(-EXtot1)
            enddo
            enddo
        print*,'-----------'
        print*,extt(39567,:)
    end subroutine lightext 

        subroutine prosp(R,w,t,prsp)
        use globlib
        real*8,intent(out) :: prsp
        real*8,intent(in) :: R,w,t
        real*8 :: wt,wt2,wt3,wt4
        real*8 :: R2,R3,R4
        real*8 :: loga,a,t1,t2,ttt
        real*8 :: a2,a4

        a = R
        wt = w*t
        wt2 = wt*wt
        wt3 = wt2*wt
        wt4 = wt3*wt
        
        R2 = R*R
        R3 = R2*R
        R4 = R3*R
        
        loga = log(a)
        a2 = a*a
        a4 = a2*a2
        t1 = -1/64*(-a4+12*R2*a2+12*a2*wt2+12*loga*R4-&
              & 24*loga*R2*wt2+12*wt4*loga-32*R3*a-&
              & 32*a*wt3)/wt3
        
        ttt = w*t

        if(ttt.gt.R) then 
          a=ttt-R
        elseif(ttt.le.R) then 
          a=R-ttt
        endif 

        if(a.le.0.0) then 
          prsp = 9999.
        else
          loga = log(a)
          a2 = a*a
          a4 = a2*a2
          t2 = -1/64*(-a4+12*R2*a2+12*a2*wt2+12*loga*R4-&
                & 24*loga*R2*wt2+12*wt4*loga-32*R3*a-&
                & 32*a*wt3)/wt3
          prsp = t1-t2
        endif 
        return 
        end subroutine prosp  

      end module 
