! predefined particle.bp file and allows the more flexible release of
! particles in a predefined region or along the closed land boundaries
! to mimik the spawning of smelt eggs, which is known to happen in these
! areas. 

!   Data type consts
      module kind_par
              implicit none 
              integer, parameter :: sng_kind1=4
              integer, parameter :: dbl_kind1=8
              real(kind=dbl_kind1), parameter :: small=1.e-6 !smallnon-neg number 
      end module kind_par

      program ptrack_coast
      use globlib
      use netcdf
      use compute_zcor
      use schism_geometry_mod 
      use ibm 
      use kind_par
      implicit real(kind=dbl_kind1) (a-h,o-z),integer(i-n)

      character(len=40),save :: file63        
      real(kind=sng_kind) :: floatout,floatout2
      real(kind=dbl_kind1), allocatable ::xpar(:),ypar(:),st_p(:),upar(:),vpar(:),wpar(:),zpar(:)
      real(kind=dbl_kind1), allocatable :: ztmp(:),ztmp2(:),dhfx(:),dhfy(:),dhfz(:),grdx(:),grdy(:), &
     &grdz(:),amas(:),wndx(:),wndy(:),timeout(:)
      integer,allocatable :: idpar(:)
      real*8,allocatable ::tpar(:),spar(:),trbpar(:),metabr(:)
      real*8,allocatable ::extpar(:),preypar(:),drycount(:)
      real*8,allocatable :: etaap(:),kbbpp(:),swspd(:)
      real*8,allocatable :: xpar_hot(:),ypar_hot(:),zpar_hot(:)
      real*8, allocatable :: spwnxlim(:,:),spwnylim(:,:)
      real*8,allocatable :: EggDev_hot(:)
      integer,allocatable :: ida(:),levpar_hot(:),ielpar_hot(:)
      integer, allocatable :: ielpar(:),levpar(:),iabnorm(:),ist(:),inbr(:)
      integer, allocatable :: idp(:),parstat(:),dim_maxpar(:)
      real(kind=dbl_kind1):: vxl(4,2),vyl(4,2),vzl(4,2),vxn(4),vyn(4),vzn(4),arco(3), &
     &dx(10),dy(10),dz(10),val(4,2),vbl(4,2),vcl(4,2),vdl(4,2),van(4),vcn(4),vdn(4),vwx(4),vwy(4)
      real*8, allocatable :: real_ar(:,:),lifeq(:)
      real*8::vmizol(4,2),vmezol(4,2),preyl(4,2)
      real*8 :: voxyl(4,2),extl(4,2),vdetrl(4,2),vdoml(4,2)
      real*8 :: mizon(4),mezon(4),detrin(4),nh4n(4)
      real*8 :: oxyn(4),extn(4),preyn(4),domn(4)
      real*8 :: nstart,lifeq_fac,rsl_ref,time_start(1)
      real*8,allocatable::mizopar(:),mezopar(:),dompar(:),detripar(:)
      real*8,allocatable::oxypar(:),weightpar1(:),weightpar2(:)
      real*8,allocatable :: lengthpar1(:),lengthpar2(:)
      real*8,allocatable :: N_indi(:),EggHatchD(:),EggHatchL(:),Npar(:)
      real*8 :: templ(4,2),saltl(4,2),tempn(4),saltn(4),outtime(1),N_lim
      integer ::node12(3),numpart,day(1),temp_id,salt_id,light_id,ibdt
      integer :: varid1,varid2,dimids(3),istat,nvtx,iret,ndeltp_ini
      integer :: ibet,turb_year_loc,len_char
      integer,allocatable:: turbout(:)
      character(len=200):: file64,basePath
      integer::ivarid1,ivarid2,ivarid3,ivarid4,ivarid5,ivarid6,ivarid7,ivarid8,ivarid9,ivarid10
      integer ::ivarid,spwnnum,mid,ivarid11,ivarid12,ivarid13,ivarid14,ivarid15
      integer ::ivarid16,ivarid17,ivarid18,ivarid19,ivarid20,ivarid21,ivarid22
      integer ::ivarid23,ivarid24,ivarid25,ifile_hot,timei_hot
      integer ::ivarid26,ivarid27,ivarid28,ivarid29,ivarid30,ivarid31
      integer ::x_dim,ncid2,idimids2(2),t_dimid,x_dimid,iret2,time_varid,t_dim
      integer ::nctd,turbid,ivarid32,ivarid33,ivarid34,ivarid35
      integer :: itime_id,flag_id,detri_id,dom_id,diat_id,oxy_id,mezo_id,mizo_id
      integer :: smibm,nspawns,maxnpar,parind,istart(2),icount(2)
      integer ::timing,count,count_rate,count_max,count2,count_rate2,count_max2
      integer:: timing2,c3,cr3,cm3,id,retention
      integer:: irec_hot,larvae_step,start_day
      integer :: iipr,preymode,prmin,prmax,princr
      integer :: spwnm,ihot_start,stg_start,itrb
      real*8,allocatable :: wpr(:),fpr(:),plx(:)
      real*8:: ret_speed,ret_dir
!     insert time tracking logicals to determine the runtime of
!     different code parts included in the particle Tracking
!     If timing=1 then timetracking is activated 
      timing = 0
      timing2 = 0
      ! initializing ibmskip 
      ibmskip=0
      !random seed used for oil spill model 
      iseed=5
      !Ekman Effect 
      rotate_angle=3.0*(pi/180.0)
      drag_c=0.03 
      !Evaporation const
      T_half=86400
      remain_ratio=0.6
      day(1) = 0
      mamid = 0
      !Cylindical index
      do k=3,4 !elem.type
        do i=1,k !local index
          do j=1,k-1 !offset
          nxq(j,i,k)=i+j
          if(nxq(j,i,k)>k) nxq(j,i,k)=nxq(j,i,k)-k
          if(nxq(j,i,k)<1.or.nxq(j,i,k)>k) then 
            write(*,*)'nx wrong',i,j,k,nxq(j,i,k)
            stop
          endif
          enddo
        enddo
      enddo 

!     Open a new output file to determine runtime differences of the
!     different parts of the tracking
!      open(100,file='time_tracking.txt',status='replace')  
      larvae_step = 0 
      open(11,file='fort.11',status='replace')
      
      smibm = 1
      open(95,file='parameter.bp',status='old')
      read(95,*)
      read(95,*) nscreen
!     Model #: 0-passive; 1:oil spill
      read(95,*) mod_part
      if(mod_part<0.or.mod_part>1) stop 'Unknown model'
      read(95,*) ibf,ibmskipmax
      if(iabs(ibf)/=1) then
        write(*,*)'Wrong ibf',ibf
        stop
      endif
      read(95,*) ihot,start_day
      read(95,*) preymode,prmin,prmax,princr
      iipr = (prmax-prmin)/princr+1
      print*,'iipr',iipr
      print*,'ihot',ihot
      if(mod_part==1.and.ibf/=1) stop 'Oil spill must have ibf=1'

      read(95,*) istiff !1: fixed distance from F.S.
      if(istiff/=0.and.istiff/=1) then
        write(*,*)'Wrong istiff',istiff
        stop
      endif
      read(95,*) ics,slam0,sfea0
      slam0=slam0/180*pi
      sfea0=sfea0/180*pi
      read(95,*) h0,rnday,dtm,nspool,ihfskip,ndeltp_ini !# of sub-divisions
      print*,dtm,'dtm after read',nspool
      if(mod(ihfskip,nspool)/=0) then
        write(*,*)'ihfskip must be a multiple of nspool'
        stop
      endif
      print*,rnday,'rnday'
      if(mod_part==1) then 
         read(95,*) !comment line 
         read(95,*) ihdf,hdc,horcon
         read(95,*) ibuoy,iwind
         read(95,*) pbeach
      endif ! mod_part=1
      read(95,*) retention,rsl_ref
      print*,'retention=',retention
      read(95,*) itrb,turb_year_loc
      if(itrb==1) then  
      print*,'tubdity forcing =  ',turb_year_loc
      endif
      close(95)
      
      rsl_ref = rsl_ref/86400. !from m/day to m/s 
      ! reading in the particle file
      open(94,file='particles.bp',status='old')
      read(94,*)  maxnpar
      print*,maxnpar
      allocate(ida(nspawns),spwnxlim(nspawns,2),spwnylim(nspawns,2),xpar(maxnpar),&
      & ypar(maxnpar),zpar(maxnpar),idp(maxnpar),ielpar(maxnpar),levpar(maxnpar),upar(maxnpar),&
      & vpar(maxnpar),wpar(maxnpar),iabnorm(maxnpar),ist(maxnpar),inbr(maxnpar),dhfx(maxnpar),&
      & dhfy(maxnpar),dhfz(maxnpar),grdx(maxnpar),grdy(maxnpar),grdz(maxnpar),amas(maxnpar),&
      &wndx(maxnpar),wndy(maxnpar),parstat(maxnpar),st_p(maxnpar),dim_maxpar(maxnpar),&
      &idpar(maxnpar),N_indi(maxnpar),Npar(maxnpar),EggHatchD(maxnpar),EggHatchL(maxnpar),&
      & weightpar1(maxnpar),weightpar2(maxnpar),drycount(maxnpar),stat=istat)
      allocate(lengthpar1(maxnpar),lengthpar2(maxnpar),kbbpp(maxnpar),detripar(maxnpar),dompar(maxnpar))
      if(istat/=0) stop 'Failed to allocate first'
      allocate(mizopar(maxnpar),mezopar(maxnpar),oxypar(maxnpar),etaap(maxnpar),swspd(maxnpar),stat=istat)
      allocate(xpar_hot(maxnpar),ypar_hot(maxnpar),zpar_hot(maxnpar),EggDev_hot(maxnpar),stat=istat)
      allocate(levpar_hot(maxnpar),ielpar_hot(maxnpar))
        if(istat/=0) stop 'Failed to alloc (1)'        
        allocate(tpar(maxnpar),spar(maxnpar),extpar(maxnpar),preypar(maxnpar),lifeq(maxnpar),trbpar(maxnpar),metabr(maxnpar))
      swspd(:) = 0.0
      trbpar(:)=0.0
      drycount(:) = 0.0
      levpar = -99 ! vertical level 
      iabnorm = 0 ! abnormal tracking exit flag 
      print*,'levpar'
      dt = dtm*nspool
      print*,'dtm',dtm,'nspool',nspool
      st_m = (ibf+1)/2*rnday*86400
      print*,'st_m',st_m,rnday
      if(ihot==0) then 
      do i=1,maxnpar
         if(ics.eq.1) then 
           read(94,*) idpar(i),st_p(i),xpar(i),ypar(i),zpar(i)
         endif  
        if(st_p(i)<0 .or.st_p(i)>rnday*86400) then
           print*,'starting time for',i,'is out of range'
          stop 
         endif 
         if(ibf*st_p(i)<ibf*st_m) st_m=st_p(i)
         enddo
         print*,'stp(1:20)',st_p(1:20)
         print*,xpar
         print*,'-----xpar'
       elseif(ihot==1) then 
        print*,'dtm',dtm,nspool
        print*,'time_start before',time_start(1)
        call read_hotstart_position(maxnpar,time_start(1),ifile_hot,xpar(:),ypar(:),zpar(:),levpar_hot(:),ielpar_hot(:),Npar(:),idpar(:),st_p(:),timei_hot,ihot,ihot_start)
        print*,'dtm',dtm,nspool
        print*,'ihot_start',ihot_start
        print*,'levpar_hot',levpar_hot(1:10)
        levpar(:) = levpar_hot(:)
        print*,'hotstart read'
          timei_hot = timei_hot-1
        print*,'timei_hot',timei_hot,dtm,nspool
        print*,'ifile_hot',ifile_hot
        print*,'time_start after',time_start(1)
        if(ibf*time_start(1)<ibf*st_m) st_m=time_start(1)
        print*,'stp(1:20)',st_p(1:20)
        print*,'xpar hotstart',xpar_hot(1),ypar_hot(1),Npar(i),idpar(1)
      endif 
      
        do i=1,maxnpar
        if(xpar(i).gt.0.0) then 
          parstat(i)=1
          print*,'defined for i',i
        else
          parstat(i) = 0
          print*,'no xpar defined,parstat(i)=0',i
        endif 
      enddo
     ! changed things, so that the oil spill only controlls the
     ! stranding but particles keep being reflected throughout the run 
     !additional parameters for Oil spill 
      ist=0 !particle status flag
      inbr=0 !neighbouring elemnt 
      
      if(mod_part==1) then 
        if(ibuoy==1) then 
          !compute the rising velocity of particles 
          gr=9.8
          rho_o=1200.0d3
          rho_w=1025.0d3
          di=500.0d-6
          dmu=1.05d-6
          !critical diameter
          dc=9.52*smu**(2./3.)/(gr**(1./3.)*(1.-rho_o/rho_w)**(1./3.))!m
          !compute the rising velocity 
          if(di>=dc) then 
            rsl=sqrt(8./3.*gr*di*(1.-rho_o/rho_w))
          else 
            rsl=sqrt(8./3.*gr*di*(1.-rho_o/rho_w))
          endif 
        else if(ibuoy==0) then
          rsl=rsl_ref
          print*,'rls',rsl
        else
          write(11,*) 'ibuoy is incorrect'
          stop
        endif !ibuoy
      endif
      !initialising the smelt ibm if smibm ==1 
      if(smibm==1) then 
        if(ihot==0) then 
          call smelt_initialising(maxnpar,stg_start,EggHatchL,lengthpar1,lengthpar2,weightpar1,weightpar2)
          print*,'length after ini',lengthpar1(1:10),lengthpar2(1:10)
          lengthpar1=lengthpar2
          weightpar1=weigthpar2
        elseif(ihot==1) then !hotstart for the values of the IBM
          call smelt_initialising(maxnpar,stg_start,EggHatchL,lengthpar1,lengthpar2,weightpar1,weightpar2)
          call smelt_hotstart(maxnpar,EggDev_hot,EggHatchD,EggHatchL,weightpar2,lengthpar2,YolkDev2,istage)
          EggDev2 = EggDev_hot
          do i=1,maxnpar
          swspd(i) = 181.15/(1+exp(-(lengthpar2(i)-29.52)/5.52))
          enddo
        endif 
      endif 
      nstart = 1000000.
      if(ihot==0) then 
         N_indi(:) = nstart
         istage(:)=stg_start
         print*,'initi. smelt stages',stg_start
      elseif(ihot==1) then 
         N_indi(:) = Npar
         print*,'min stage',minval(istage(:))
      endif  
      N_lim = nstart*0.01!# of individuals per particle
      EggHatchD(:) = 0.0
      nrec=ihfskip/nspool !# of records (steps) per stack
      dt=dtm*nspool !output time step
      print*,dtm,nspool,dt
      ntime=int(rnday*86400./dt) !total # of records
      print*,ntime,'ntime',rnday*86400.,dt
      print*,st_m,'stm' 
      do i=1,ntime/nrec+1
        if(st_m/dt>(i-1)*nrec.and.st_m/dt<=i*nrec) then
          ifile=i
          print*,'i=',i,st_m/dt,(i-1)*nrec,st_m
          exit
        endif
      enddo
      print*, 'Starting stack =',ifile,st_m,ntime
      if(ibf==1) then
        if(ihot==0) then       
          iths=(ifile-1)*nrec+1 !starting _total_ record # 
        elseif(ihot==1) then 
          iths = ((ifile-1)*nrec+1)
          print*,'iths_wo',iths,ifile,nrec
          iths = iths+timei_hot
          irec_hot = int(timei_hot - iths)      
          print*,'irec_hot',irec_hot,'iths',iths
          print*,'timei_hot',timei_hot
        endif 
      else !ibf=-1
        iths=ifile*nrec !starting _total_ record #
      endif
      print*, 'Starting step # =',iths
      print*,'nrec',nrec
      write(file63,'("schout_",I0,".nc")') ifile
      print*,'ifile',ifile
      write(turbyear_char,'(i12)') turb_year_loc
      ifile_char=adjustl(ifile_char); len_char=len_trim(ifile_char)
      turbyear_char = adjustl(turbyear_char)
      ten_char =len_trim(turbyear_char)
      ! Define the base path with the year part
      basePath ='/work/gg0877/g260205/TurbForcing/turb_' // turbyear_char(1:ten_char) // '/turbout_'
      print*,'basePath',basePath
      ! Construct file64 with the formatted output
      write(file64, '(A, I0, ".nc")') trim(basePath), ifile
      print*,'file64',file64
      print*,'file',file63
      !opening the turbulence file
      ibet=nf90_open(trim(adjustl(file64)),OR(NF90_NETCDF4,NF90_NOWRITE),nctd)
      if(ibet/=nf90_NoErr) stop 'could not open turbulence file'
      ibet=nf90_inq_varid(nctd,'turbidity',turbid)
      if(ibet/=nf90_NoErr) stop 'could not inq turb var'
      !opening normal schism output 
      iret=nf90_open(trim(adjustl(file63)),OR(NF90_NETCDF4,NF90NOWRITE),ncid)
      if(iret/=nf90_NoErr) stop '1st stack not opened'
      iret=nf90_inq_varid(ncid,'elev',ielev_id)
      if(iret/=nf90_NoErr) stop 'elev not found'
      iret=nf90_inq_varid(ncid,'hvel',luv)
      if(iret/=nf90_NoErr) stop 'hvel not found'
      iret=nf90_inq_varid(ncid,'vertical_velocity',lw)
      if(iret/=nf90_NoErr) stop 'w not found'
      if(mod_part==1) then
        iret=nf90_inq_varid(ncid,'wind_speed',lwind)
        if(iret/=nf90_NoErr) stop 'wind not found'
        iret=nf90_inq_varid(ncid,'diffusivity',ltdff)
        if(iret/=nf90_NoErr) stop 'tdiff not found'
      endif !mod_part
      if(smibm==1) then !if ibm is included
              iret=nf90_inq_varid(ncid,'temp',temp_id)
              if(iret/=nf90_NoErr) stop 'temp not found'
              iret=nf90_inq_varid(ncid,'salt',salt_id)
              if(iret/=nf90_NoErr) stop 'salt not found'
              iret=nf90_inq_varid(ncid,'solar_radiation',light_id)
              if(iret/=nf90_NoErr) stop 'light not read'
              iret=nf90_inq_varid(ncid,'ECO_fla',flag_id)
              if(iret/=nf90_NoErr) stop 'flag not inq'
              iret=nf90_inq_varid(ncid,'ECO_det',detri_id)
              if(iret/=nf90_NoErr) stop 'det not inq'
              iret=nf90_inq_varid(ncid,'ECO_dia',diat_id)
              if(iret/=nf90_NoErr) stop 'diatoms not inq'
              iret=nf90_inq_varid(ncid,'ECO_microzoo',mizo_id)
              if(iret/=nf90_NoErr) stop 'mizoo not inq'
              iret=nf90_inq_varid(ncid,'ECO_mesozoo',mezo_id)
              if(iret/=nf90_NoErr) stop 'meso not inq'
              iret=nf90_inq_varid(ncid,'ECO_dom',dom_id)
              if(iret/=nf90_NoErr) stop 'dom not inq'
              iret=nf90_inq_varid(ncid,'ECO_oxy',oxy_id)
              if(iret/=nf90_NoErr) stop 'oxy not inq'
      end if ! ibm
       
      iret=nf90_inq_dimid(ncid,'nSCHISM_vgrid_layers',i)
      if(iret/=nf90_NoErr) stop 'nSCHISM_vgrid_layers not found'
      iret=nf90_Inquire_Dimension(ncid,i,len=nvrt)
      print*,'nvrt after read',nvrt
      if(iret/=nf90_NoErr) stop 'read error(1)'
      iret=nf90_inq_varid(ncid,'SCHISM_hgrid_face_nodes',varid1)
      if(iret/=nf90_NoErr) stop 'read error(2)'
      iret=nf90_Inquire_Variable(ncid,varid1,dimids=dimids(1:2))
      if(iret/=nf90_NoErr) stop 'read error(3)'
      iret=nf90_Inquire_Dimension(ncid,dimids(1),len=nvtx)
      if(iret/=nf90_NoErr) stop 'read error(4)'
      iret=nf90_Inquire_Dimension(ncid,dimids(2),len=ne)
      if(iret/=nf90_NoErr) stop 'read error(5)'
      if(nvtx/=4) stop 'nvtx/=4'
      iret=nf90_inq_varid(ncid,'SCHISM_hgrid_node_x',varid2)
      if(iret/=nf90_NoErr) stop 'read error(6)'
      iret=nf90_Inquire_Variable(ncid,varid2,dimids=dimids)
      if(iret/=nf90_NoErr) stop 'read error(7)'
      iret=nf90_Inquire_Dimension(ncid,dimids(1),len=np)
      if(iret/=nf90_NoErr) stop 'read error(8)'
      iret=nf90_inq_varid(ncid,'time',itime_id)
      if(iret/=nf90_NoErr) stop 'read error(9)'
      iret=nf90_Inquire_Variable(ncid,itime_id,dimids=dimids)
      if(iret/=nf90_NoErr) stop 'read error(10)'
      iret=nf90_Inquire_Dimension(ncid,dimids(1),len=nrec)
      if(iret.ne.NF90_NOERR) then 
              print*, nf90_strerror(iret)
              stop 'readheader: error reading header'
      endif
      print*,'np=', np 
      allocate(x(np),y(np),dp(np),kbp00(np),i34(ne),elnode(4,ne),timeout(nrec), &
              &area(ne),nne(np),idry(np),idry_e(ne),idry_e0(ne),kbp(np),kbe(ne))
      allocate(eta1(np),eta2(np),eta3(np),xctr(ne),yctr(ne),isbnd(np),wnx1(np), &
              &wnx2(np),wny1(np),wny2(np),dldxy(3,2,ne),real_ar(nvrt,np),stat=istat)
      if(istat/=0) stop 'failed to allocate(3)'
      allocate(prey2(np,nvrt),prey1(np,nvrt),fpr(iipr),wpr(iipr),plx(iipr),turbout(nrec))
      prey2(:,:) = 0.0
      prey1(:,:) = 0.0
      fpr = 0.0
      wpr = 0.0
      plx = 0.0
       
      iret=nf90_get_var(ncid,varid1,elnode)
      if(iret/=nf90_NoErr) stop 'read error(11)'
      iret=nf90_get_var(ncid,varid2,x) 
      if(iret/=nf90_NoErr) stop 'read error(12)'
      iret=nf90_inq_varid(ncid,'SCHISM_hgrid_node_y',varid1) 
      if(iret/=nf90_NoErr) stop 'read error(13)'
      iret=nf90_get_var(ncid,varid1,y)
      if(iret/=nf90_NoErr) stop 'read error(14)'
      iret=nf90_inq_varid(ncid,'depth',varid1) 
      if(iret/=nf90_NoErr) stop 'read error(15)'
      iret=nf90_get_var(ncid,varid1,dp)
      if(iret/=nf90_NoErr) stop 'read error(16)'
      iret=nf90_inq_varid(ncid,'node_bottom_index',varid1)
      if(iret/=nf90_NoErr) stop 'read error(17)'
      iret=nf90_get_var(ncid,varid1,kbp00)
      if(iret/=nf90_NoErr) stop 'kbp00 not found'
      !iret=nf90_close(ncid)
      !calc i34 (number of elements in elnode) 
      i34 = 4 !initialising
      do i=1,ne
      if(elnode(4,i)<0) i34(i)=3
      enddo
      !read in h- and v-grid and compute the geometry
      !do not read in conn table as binary may split quads 
      if(ics==1) then 
              open(14,file='hgrid.gr3',status='old')
      else
              open(14,file='hgrid.ll',status='old')
      endif
      read(14,*)
      read(14,*) ne2,np2
      if(np/=np2) stop 'mismatch(3)'
      do i=1,np
      if(ics==1) then 
              read(14,*) j,x(i),y(i),dp(i)
      else if(ics==2) then 
              read(14,*) j,xlon,ylat,dp(i)
              ylat=ylat/180*pi
              xlon=xlon/180*pi
              call cpp(x(i),y(i),xlon,ylat,slam0,sfea0)
      endif  
      enddo !i=1:np

      do i=1,ne2
      !Use conn table from binary 
      read(14,*) !j,i34(i),elnode(1:i34(),i)
      enddo

      do i=1,ne
      n1=elnode(1,i)
      n2=elnode(2,i)
      n3=elnode(3,i)
      area(i)=signa(x(n1),x(n2),x(n3),y(n1),y(n2),y(n3))

      !derivative of shape functions for triangles only 
      !for quads use nodes 1-3 
      do j=1,3
      id1=j+1
      id2=j+2
      if(id1>3) id1=id1-3
      if(id2>3) id2=id2-3
      dldxy(j,1,i)=(y(elnode(id1,i))-y(elnode(id2,i)))/2/area(i)
      dldxy(j,2,i)=(x(elnode(id2,i))-x(elnode(id1,i)))/2/area(i)
      enddo !j

      if(i34(i)==4) then 
              n4=elnode(4,i)
              area(i)=area(i)+signa(x(n1),x(n3),x(n4),y(n1),y(n3),y(n4))
      endif

      if(area(i)<=0) then 
              write(11,*) 'Negative area at',i
              stop
      endif 
      enddo 

      ! Open bnds (not affected by splitting of quads)
      isbnd=0
      read(14,*) nope 
      read(14,*) neta
      ntot=0
      do k=1,nope
      read(14,*) nond
      do i=1,nond
      read(14,*) iond
      isbnd(iond)=k
      enddo 
      ntot=ntot+nond
      enddo 

      if(neta/=ntot) then 
              write(11,*) 'neta/= total # of open bnd nodes',neta,ntot
              stop 
      endif 

      !Land bnds 
      read(14,*) nland 
      read(14,*) nvel
      do k=1,nland
      read(14,*) nlnd
      do i=1,nlnd
      read(14,*) ilnd
      if(isbnd(ilnd)==0) isbnd(ilnd)=-1 !overlap of open bounds
      enddo 
      enddo

      close (14) !close hgrid 

      !vgrid 
      open(19,file='vgrid.in',status='old')
      read(19,*) ivcor 
      read(19,*) nvrt
      rewind(19)
      allocate(sigma_lcl(nvrt,np),z(np,nvrt),icum1(np,nvrt),icum2(np,nvrt,2), &
     &uu1(np,nvrt),vv1(np,nvrt),ww1(np,nvrt),uu2(np,nvrt),vv2(np,nvrt),ww2(np,nvrt), &
     &ztmp(nvrt),ztmp2(nvrt),sigma(nvrt),ztot(nvrt),hf1(np,nvrt), &
     &hf2(np,nvrt),vf1(np,nvrt),vf2(np,nvrt),hvis_e(ne,nvrt),temp1(np,nvrt),&
     &salt1(np,nvrt),temp2(np,nvrt),salt2(np,nvrt),stat=istat)
     allocate(flage1(np,nvrt),flage2(np,nvrt),diat1(np,nvrt),diat2(np,nvrt),mizoo1(np,nvrt),mizoo2(np,nvrt),&
     &mezoo1(np,nvrt),mezoo2(np,nvrt),detri1(np,nvrt),detri2(np,nvrt),nh41(np,nvrt),nh42(np,nvrt),&
     &no31(np,nvrt),no32(np,nvrt),po42(np,nvrt),po41(np,nvrt),sil1(np,nvrt),sil2(np,nvrt),&
     &oxy2(np,nvrt),oxy1(np,nvrt),light2(np),ext1(np,nvrt),ext2(np,nvrt),&
     &dom2(np,nvrt),dom1(np,nvrt),stat=istat)
     if(istat/=0) stop 'Failed to alloc(3)'
     call get_vgrid_double('vgrid.in',np,nvrt,ivcor,kz,h_s,h_c,theta_b,theta_f,ztot,sigma,sigma_lcl,kbp)
     ! Init some arrays (for below bottom etc) 
     uu2=0; vv2=0; ww2=0; vf2=0; hf2=0; wnx2=0;wny2=0;temp2=0;salt2=0

!***********************************************************************************************
!
!                               Compute Geometry
!                     
!***********************************************************************************************

     ! We also need elem ball 
      nne=0
      do i=1,ne
        do j=1,i34(i)
          nd=elnode(j,i)
          nne(nd)=nne(nd)+1
        enddo 
      enddo 
      mnei=maxval(nne)
      allocate(indel(mnei,np),stat=istat)
      if(istat/=0) stop 'Failed to alloc. indel'
      nne=0
      do i=1,ne
        do j=1,i34(i)
          nd=elnode(j,i)
          nne(nd)=nne(nd)+1
          if(nne(nd)>mnei) then 
            write(11,*)'Too many neighbours',nd
            stop 
          endif 
          indel(nne(nd),nd)=i
        enddo 
      enddo !i

      call compute_nside(np,ne,i34,elnode,ns)
      if(nscreen.eq.1) write(*,*) 'There are',ns,' sides in the grid ..'

!     Allocate side-related arrays 
      allocate(ic3(4,ne),elside(4,ne),isdel(2,ns),isidenode(2,ns),xcj(ns),ycj(ns), &
     &snx(ns),sny(ns),distj(ns),kbs(ns),dps(ns),stat=istat)
      if(istat/=0) stop 'Failed to alloc (4)'
!     Then compute the rest of side related arrays with additional 
!     inputs (xnd,ynd) (x,y coordinates of each node) 
      call schism_geometry_double(np,ne,ns,x,y,i34,elnode,ic3,elside,isdel,isidenode,xcj,ycj)
      !Remaining side arrays 
      do i=1,ns
        nd1=isidenode(1,i)
        nd2=isidenode(2,i)
        dps(i)=(dp(nd1)+dp(nd2))/2
        distj(i)=dsqrt((x(nd2)-x(nd1))**2+(y(nd2)-y(nd1))**2)
        if(distj(i)==0) then 
          write(11,*)'Zero side',i
          stop 
        endif 
        thetan=atan2(x(nd1)-x(nd2),y(nd2)-y(nd1))
        snx(i)=cos(thetan) !from elem 1->2
        sny(i)=sin(thetan) 
      enddo !i

!     compute element centers 
      do i=1,ne
        xctr(i)=0
        yctr(i)=0
        do j=1,i34(i)
          xctr(i)=xctr(i)+x(elnode(j,i))/i34(i)
          yctr(i)=yctr(i)+y(elnode(j,i))/i34(i)
        enddo 
      enddo !i=1,ne

      if(nscreen.eq.1) write(*,*) 'done computing geometry'
 
!...  Compute initial elements of the particle tracking 
     lp1: do i=1,maxnpar
        do k=1,ne
          call pt_in_poly2(i34(k),x(elnode(1:i34(k),k)),y(elnode(1:i34(k),k)),xpar(i),ypar(i),inside)
          if(inside/=0) then 
            ielpar(i) = k
            !check with ielpar_hot
            if(ihot==1) then 
            if (ielpar(i)/=ielpar_hot(i)) then 
               print*,'ielpar not matching'
               print*,ielpar(i),ielpar_hot(i),i
            endif 
            endif
            cycle lp1
          endif
        enddo 
        print*,'Cannot find init. element for particle',i
        parstat(i) = 0 !particle is dead now
        istage(i)=4
        end do lp1 !i=1,nparticles 
       
       open(102,file='xpar_pos_debug.txt',status='replace')
       close(102)
       open(103,file='tpar_debug.txt',status='replace')
       close(103)
       open(299,file='larvae_debug.txt',status='replace')
       close(299)
       open(104,file='output_debug.txt',status='replace')
       close(104)
       open(105,file='vert_speed_log.txt',status='replace')
       close(105)

     ! open the netcdf file for the output of the tracking 
       x_dim = int(maxnpar)
       print*,'x_dim',x_dim
       dim_maxpar = (/1:int(maxnpar)/)
       if (ihot == 0) then !nur erstellen der Datei wenn kein hotstart 
       iret2 = nf90_create('ibm_output.nc',NF90_CLOBBER,ncid2)
       !define the dimensions 
       iret2=nf90_def_dim(ncid2,'npar',x_dim,x_dimid)
       iret2=nf90_def_dim(ncid2,'time',nf90_unlimited,t_dimid)
       idimids2=(/x_dimid,t_dimid/)
       iret2=nf90_def_var(ncid2,'idPar',NF90_INT,idimids2,ivarid)
       iret2=nf90_def_var(ncid2,'time',NF90_REAL,t_dimid,ivarid11)
       iret2=nf90_def_var(ncid2,'pstage',NF90_INT,idimids2,ivarid10)
       if(iret2/=nf90_noerr) stop 'pstage id not def' 
       iret2=nf90_def_var(ncid2,'N_ind',NF90_REAL,idimids2,ivarid24) 
       iret2=nf90_def_var(ncid2,'npar',NF90_REAL,x_dimid,ivarid12)
       if(iret2/=nf90_noerr) stop 'nparid not def'  
       iret2=nf90_def_var(ncid2,'xpar',NF90_REAL,idimids2,ivarid1)
       if(iret2/=nf90_noerr) stop 'xparid not def'  
       iret2=nf90_def_var(ncid2,'ypar',NF90_REAL,idimids2,ivarid2)
       if(iret2/=nf90_noerr) stop 'ypar id not def'  
       iret2=nf90_def_var(ncid2,'zpar',NF90_REAL,idimids2,ivarid3)
       if(iret2/=nf90_noerr) stop 'zparid not def'  
       iret2=nf90_def_var(ncid2,'tpar',NF90_REAL,idimids2,ivarid4)
       if(iret2/=nf90_noerr) stop 'tparid not def'  
       iret2=nf90_def_var(ncid2,'spar',NF90_REAL,idimids2,ivarid5)
       if(iret2/=nf90_noerr) stop 'sparid not def'  
       iret2=nf90_def_var(ncid2,'upar',NF90_REAL,idimids2,ivarid7)
       if(iret2/=nf90_noerr) stop 'uparid not def'  
       iret2=nf90_def_var(ncid2,'vpar',NF90_REAL,idimids2,ivarid8)
       if(iret2/=nf90_noerr) stop 'vparid not def'  
       iret2=nf90_def_var(ncid2,'wpar',NF90_REAL,idimids2,ivarid9)
       if(iret2/=nf90_noerr) stop 'wpar id not def' 
       iret2=nf90_def_var(ncid2,'mizopar',NF90_REAL,idimids2,ivarid15)
       if(iret2/=nf90_noerr) stop 'mizopar not def'
       iret2=nf90_def_var(ncid2,'mezopar',NF90_REAL,idimids2,ivarid16)
       if(iret2/=nf90_noerr) stop 'mezopar not def'
       iret2=nf90_def_var(ncid2,'oxypar',NF90_REAL,idimids2,ivarid23)
       if(iret2/=nf90_noerr) stop 'oxypar not def'
       iret2=nf90_def_var(ncid2,'extpar',NF90_REAL,idimids2,ivarid25)
       if(iret2/=nf90_noerr) stop 'extpar not def'
       iret2=nf90_def_var(ncid2,'ielpar',NF90_INT,idimids2,ivarid26)
       if(iret2/=nf90_noerr) stop 'ielpar not def'
       iret2=nf90_def_var(ncid2,'levpar',NF90_INT,idimids2,ivarid27)
       if(iret2/=nf90_noerr) stop 'levpar not def'
       iret2=nf90_def_var(ncid2,'etap',NF90_REAL,idimids2,ivarid28)
       if(iret2/=nf90_noerr) stop 'etap not def'
       iret2=nf90_def_var(ncid2,'kbpp',NF90_INT,idimids2,ivarid29)
       if(iret2/=nf90_noerr) stop 'kbp not def'
       iret2=nf90_def_var(ncid2,'preypar',NF90_REAL,idimids2,ivarid30)
       if(iret2/=nf90_noerr) stop 'preyp not def'
       iret2=nf90_def_var(ncid2,'turbpar',NF90_REAL,idimids2,ivarid31)
       if(iref2/=nf90_noerr) stop 'turbpar not def'
       iret2=nf90_def_var(ncid2,'drycount',NF90_REAL,idimids2,ivarid32)
       if(iret2/=nf90_noerr) stop 'dryocunt not def'
       iret2=nf90_def_var(ncid2,'grdy',NF90_REAL,idimids2,ivarid33)
       if(iret2/=nf90_noerr) stop 'grdy not def'
       iret2=nf90_def_var(ncid2,'grdx',NF90_REAL,idimids2,ivarid34)
       if(iret2/=nf90_noerr) stop 'grdx not def'
       iret2=nf90_def_var(ncid2,'dhx',NF90_REAL,idimids2,ivarid35)
       if(iret2/=nf90_noerr) stop 'dhx not def'
       iret2=nf90_enddef(ncid2)
       iret2=nf90_put_var(ncid2,ivarid12,dim_maxpar)
       if(iret2/=nf90_noerr) stop 'xpar not written'  
       iret2=nf90_close(ncid2)
       call create_netcdf_output_ibm(maxnpar)
                              endif
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                                                                       !
!                              Time Iterattion                                          !
!                                                                                       !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      upar=0.; vpar=0.; wpar=0.;tpar=0.;spar=0.;
      mizopar = 0.;mezopar=0.; oxypar =0.
      !starting record # in the starting stack 
      if(ibf==1) then
        if(ihot==0) then  
          it2 = ntime 
          irec1 = 1
        elseif(ihot==1) then 
          it2=ntime
          irec1 = time_ihot+1
        endif
      else 
        it2 = 1
        irec1 = nrec
      endif
      print*,'ntime',ntime
      print*,'it2',it2,iths
      print*,'dt*ibf',dt,ibf,nrec 
      ibdt = 1*60*60/(dt*ibf)
      do it1=iths,it2,ibf !it is total time record #
      !-------------------------------------------------
      time = it1*(dt)
      print*,'it1',it1,'dt',dt,'time',time,it2,ibf
!..   read in elevation and vel. info 
      if((ibf==1.and.it1.gt.nrec*ifile).or.(ibf==-1.and.it1<=nrec*(ifile-1)))then 
              !open next stack i (after a full file is completed)
              print*,'getting new ele info',irec1,nrec
              iret=nf90_sync(ncid)
              iret=nf90_close(ncid)
              print*,'after closing'
              ifile=ifile+ibf !stck #
              print*,'ifile',ifile
              write(file63,'("schout_",I0,".nc")') ifile
              file64='/work/gg0877/g260205/TurbForcing/turbout_'//turbyear_char(1:ten_char)//'/turbout_'//ifile_char(1:len_char)//'.nc'
              print*,file63
              print*,file64
              iret=nf90_open(trim(adjustl(file63)),OR(NF90_NETCDF4,NF90_NOWRITE),ncid)
              print*,'after opening'
              ibet=nf90_open(trim(adjustl(file64)),OR(NF90_NETCDF4,NF90_NOWRITE),nctd)
              !time is double 
              iret=nf90_inq_varid(ncid,'time',itime_id)
              if(iret/=nf90_NoErr) stop 'itime_id'
              iret=nf90_get_var(ncid,itime_id,timeout,(/1/),(/nrec/))
              if(iret/=nf90_NoErr) stop 'time not read'
              print*,'timeout',timeout
              !Reset record #
              if(ibf==1) then 
                      irec1=1
              else 
                      irec1=nrec
              endif
      endif !ibf

      ! calculate the number of steps that are actually read in 
      ! assuming that the timestep is 5 min every 4th timestep is
      ! actually read. in all other steps the fields are kept the same 
      print*,irec1,nrec,'iret1,nrec'
      print*,timeout
      if (irec1.lt.1.or.irec1.gt.nrec) stop 'record out of bound'
      if (ibmskip == ibmskipmax .or. irec1==1 .or. irec1==timei_hot+1) then 
      print*,'reading in new data of the nc files'
      ibet=nf90_get_var(nctd,turbid,turbout,(/1/),(/nrec/))
      print*,'nctd',nctd,'int_ar',turbout,'ibet',ibet
      print*,file64
      print*,'turbulence value',real(turbout(irec1))
      print*,shape(turbout)
      print*,'file 63',file63
      if(iret/=nf90_NoErr) stop 'open not working'
      iret=nf90_get_var(ncid,ielev_id,real_ar(1,1:np),(/1,irec1/),(/np,1/))
      print*,iret
      if(iret/=nf90_NoErr) stop 'elev not read'
      eta2=real_ar(1,1:np)
      print*,'eta read'
      iret=nf90_get_var(ncid,luv,real_ar(1:nvrt,1:np),(/1,1,1,irec1/),(/1,nvrt,np,1/))
      print*,'iret u',iret
      if(iret/=nf90_NoErr) stop 'uu2 not read'
      uu2(:,:)=transpose(real_ar(1:nvrt,1:np))
      print*,'uu2 read' 
      iret=nf90_get_var(ncid,luv,real_ar(1:nvrt,1:np),(/2,1,1,irec1/),(/1,nvrt,np,1/))
      if(iret/=nf90_NoErr) stop 'vv2 not read'
      vv2(:,:)=transpose(real_ar(1:nvrt,1:np))
      print*,'vv2 read'
      iret=nf90_get_var(ncid,lw,real_ar(1:nvrt,1:np),(/1,1,irec1/),(/nvrt,np,1/))
      ww2(:,:)=transpose(real_ar(1:nvrt,1:np))
      print*,'ww2 read'
      if(mod_part==1) then 
        wnx2(:) = 0;
        wny2(:) = 0;
        iret=nf90_get_var(ncid,ltdff,real_ar(1:nvrt,1:np),(/1,1,irec1/),(/nvrt,np,1/))
        if(iret/=nf90_NoErr) stop 'tdiff not read'
        vf2(:,:)=transpose(real_ar(1:nvrt,1:np))
      endif !modpart =1
      ! reading temperature and salinity at the particles location
      if(smibm==1) then 
        iret=nf90_get_var(ncid,temp_id,real_ar(1:nvrt,1:np),(/1,1,irec1/),(/nvrt,np,1/))
        if(iret/=nf90_NoErr) stop 'temperature not read'
        temp2(:,:) = transpose(real_ar(1:nvrt,1:np))
        iret=nf90_get_var(ncid,salt_id,real_ar(1:nvrt,1:np),(/1,1,irec1/),(/nvrt,np,1/))
        if(iret/=nf90_NoErr) stop 'salt not read'
        salt2(:,:) = transpose(real_ar(1:nvrt,1:np))
        iret=nf90_get_var(ncid,light_id,real_ar(1,1:np),(/1,irec1/),(/np,1/))
        if(iret/=nf90_noerr) stop 'ligvht read'
        light2 = real_ar(1,1:np)
        iret=nf90_get_var(ncid,flag_id,real_ar(1:nvrt,1:np),(/1,1,irec1/),(/ncrt,np,1/))
        if(iret/=nf90_NoErr) stop 'flag not read'
        flage2=transpose(real_ar(1:nvrt,1:np))
        iret=nf90_get_var(ncid,diat_id,real_ar(1:nvrt,1:np),(/1,1,irec1/),(/nvrt,np,1/))
        if(iret/=nf90_NoErr) stop 'diat not read'
        diat2=transpose(real_ar(1:nvrt,1:np))
        iret=nf90_get_var(ncid,mizo_id,real_ar(1:nvrt,1:np),(/1,1,irec1/),(/nvrt,np,1/))
        if(iret/=nf90_NoErr) stop 'mizoo no read'
        mizoo2=transpose(real_ar(1:nvrt,1:np))
        iret=nf90_get_var(ncid,mezo_id,real_ar(1:nvrt,1:np),(/1,1,irec1/),(/nvrt,np,1/))
        if(iret/=nf90_NoErr) stop 'mezoo not read'
        mezoo2=transpose(real_ar(1:nvrt,1:np))
        iret=nf90_get_var(ncid,detri_id,real_ar(1:nvrt,1:np),(/1,1,irec1/),(/nvrt,np,1/))
        if(iret/=nf90_NoErr) stop 'detri not read'
        detri2=transpose(real_ar(1:nvrt,1:np))
        iret=nf90_get_var(ncid,dom_id,real_ar(1:nvrt,1:np),(/1,1,irec1/),(/nvrt,np,1/))
        if(iret/=nf90_NoErr) stop 'dom not read'
        dom2=transpose(real_ar(1:nvrt,1:np))
        iret=nf90_get_var(ncid,oxy_id,real_ar(1:nvrt,1:np),(/1,1,irec1/),(/nvrt,np,1/))
        if(iret/=nf90_NoErr) stop 'oxy not read'
        oxy2=transpose(real_ar(1:nvrt,1:np))
      endif
      print*,'reading successfull' 
      call conv_biom(mizoo2,mezoo2,fpr,plx,wpr,prey2,iipr,prmin,princr)
      if(preymode==1) then 
        prey2(:,:) = prey2(:,:)/2.
      endif 
      ibmskip=0
      endif
      trbpar(:) = real(turbout(irec1)) 
      irec1=irec1+ibf
      ibmskip=ibmskip+1
      !store info for the first step 
      if(it1==iths) eta1=eta2
      !compute elevation eta3
      do i=1,np
      if(ibf==1) then 
        eta3(i) = eta1(i)
      else 
        eta3(i) = eta2(i)
      endif 
      enddo 

      !compute z-cor 
      call levels 
      !Deal with junks
      do i=1,np
         if(idry(i)==1) then 
           uu2(i,:)=0;vv2(i,:)=0; ww2(i,:)=0;vf2(i,:)=0
           ! changed 10.07.24 -> now getting the kbp(i) value
           temp2(i,:)=temp2(i,kbp(i));salt2(i,:)=salt2(i,kbp(i));
           mizoo2(i,:)=mizoo2(i,kbp(i))
           oxy2(i,:)=oxy2(i,kbp(i))
           mezoo2(i,:)=mezoo2(i,kbp(i))
         else 
           do k=1,nvrt
              if(k<=kbp(i)-1.or.abs(uu2(i,k))>1.e8) then 
                 uu2(i,k) = 0.0
                 vv2(i,k) = 0.0
                 ww2(i,k) = 0.0
                 vf2(i,k) = 0.0
                 oxy2(i,k) = 0.0
                 temp2(i,k)=temp2(i,kbp(i))
                 salt2(i,k)=salt2(i,kbp(i))
                 mizoo2(i,k)=mizoo2(i,kbp(i))
                 mezoo2(i,k)=mezoo2(i,kbp(i))
                 prey2(i,k)=0.0
              endif 
           enddo  !k 
         endif 
      enddo !i
      !Store info for the first step 
      if(it1==iths) then 
         !print*,'test1'
         uu1=uu2;vv1=vv2;ww1=ww2
         vf1=vf2;wnx1=wnx2;wny1=wny2
         temp1=temp2;salt1=salt2
         flage1 = flage2; diat1=diat2; mizoo1=mizoo2
         detri1=detri2; nh41=nh42; no31=no32; po41=po42
         sil1=sil2;ext1=ext2;prey1=prey2;dom1=dom2
      endif
      !compute hvis_e based on Smagorinsky 
      if(mod_part==1) then 
         if(ihdf==0) then 
            hf2=hdc !constant diffusivity [m^1/s]
         else !smagorinsky 
            hvis_e=0
            do i=1,ne
               if(idry_e(i) ==1) cycle
                   do k=kbe(i),nvrt
                      dudx_e=dot_product(uu2(elnode(1:3,i),k),dldxy(1:3,1,i))
                      dudy_e=dot_product(uu2(elnode(1:3,i),k),dldxy(1:3,2,i))
                      dvdx_e=dot_product(vv2(elnode(1:3,i),k),dldxy(1:3,1,i))
                      dvdy_e=dot_product(vv2(elnode(1:3,i),k),dldxy(1:3,2,i))
                      hvis_e(i,k)=horcon*area(i)*sqrt(dudx_e**2+dvdy_e**2+0.5*(dudy_e+dvdx_e)**2)!m^2/s
                   enddo
                enddo 
                !convert to nodes 
                hf2=0
                do i=1,np
                   if(idry(i)==1) cycle
                      do k=kbp(i),nvrt
                         spm=0;slm=0
                         do j=1,nne(i)
                            nel=indel(j,i)
                            rl=sqrt((x(i)-xctr(nel))**2+(y(i)-yctr(nel))**2)
                            if(rl==0) stop 'rl=0'
                            slm=slm+1./rl
                            spm=spm+hvis_e(nel,k)/rl
                         enddo 
                         hf2(i,k) = spm/slm
                         enddo 
                      enddo
              endif

              print*,'hf2 analysis',maxval(hf2),minval(hf2)
              !Store infop for diffusion term in first time step 
              if(it1==iths) then 
                hf1=hf2;grdx=0.0;grdy=0.0;grdz=0.0
                dhfx=hdc; dhfy=hdc; dhfz=3.0d-4
              endif 
      endif 

      if(nscreen.eq.1) write(*,*)'begin ptrack..'   

      call lightext(light2,flage2,diat2,dom2,detri2,z,kbp00,ext2)
       
      do i=1,maxnpar     
         if(time<st_p(i)) then
           print*,'not yet activated'
           parstat(i)=2
         elseif(time>=st_p(i).and.parstat(i)/=0) then 
           print*,'activated',i
           print*,parstat(i),xpar(i),ypar(i)
           parstat(i)=1
         endif
         if(parstat(i) ==0) then 
             print*,'parstat(i) ==0'
           elseif(parstat(i)==1) then 
             eta_p=0;dp_p=0 !for output before moving
             pt=dt
             !if(ihot==1) then 
             !  levpar(i) = levpar_hot(i)
             ! endif 
              print*,levpar(i),'levpar(i)'
             !init starting level
             if(levpar(i)==-99) then !dry
               pt=(time-st_p(i))*ibf !just started
               print*,'just started pt',pt,time-st_p(i),st_p(i)
               !initialising the important fields for the ibm
            
               if(pt<=0) then 
                 write(*,*)'Tracking step negative',pt
                 stop
               endif
               iel=ielpar(i)
               if(idry_e(iel)==1) then !dry
                 levpar(i)=-1
               else !wet
                call pt_in_poly3(i34(iel),x(elnode(1:i34(iel),iel)),y(elnode(1:i34(iel),iel)),& 
                  &xpar(i),ypar(i),arco,nodel)
                 do k=kbe(iel),nvrt !1:21 
                   ztmp2(k)=0
                   do j=1,3 
                     nd=elnode(nodel(j),iel)
                     ztmp2(k)=ztmp2(k)+z(nd,max(k,kbp(nd)))*arco(j)
                     print*,ztmp2(k),'ztmp2(k)'
                   enddo !j
                 enddo !k
                 zpar(i) = max(zpar(i)+ztmp2(nvrt),ztmp2(kbe(iel)))
                 jlev=0
                 print*,zpar(i)
                 print*,' after zpar'
                 do k=kbe(iel),nvrt-1
                   if(zpar(i)>=ztmp2(k).and.zpar(i)<=ztmp2(k+1)) then 
                     jlev=k+1
                     exit
                   endif
                 enddo !k
                 if(jlev==0) then
                   write(11,*) 'Cannot find an init.level:',i,zpar(i),(ztmp2(k),k=kbe(iel),nvrt)
                   if(ihot==1) then 
                     print*,jlev,'jlev'
                     jlev = levpar_hot(i)
                     print*,'jlev',jlev,levpar_hot(i)
                     zpar(i) = ztmp2(jlev)
                     print*,levpar_hot(i),'zpar(i)',zpar(i)
                   else
                     print*,'stopping'
                     stop
                   endif !ihot==1 
                 endif ! jlev==0
                 levpar(i)=jlev
                 upar(i)=0;vpar(i)=0;;wndx(i)=0;wndy(i)=0
                 tpar(i)=0;spar(i)=0
                 mizopar(i)=0;mezopar(i)=0
                 oxypar(i)=0
                 extpar(i) = 0
                 dompar(i)=0
                 detripar(i)=0
                 preypar(i) = 0
                 print*,'initialising the par values' 
                 do j=1,3
                   nd=elnode(nodel(j),iel)
                   upar(i)=upar(i)+uu2(nd,jlev)*arco(j)
                   vpar(i)=vpar(i)+vv2(nd,jlev)*arco(j)
                   wpar(i)=wpar(i)+ww2(nd,jlev)*arco(j)
                   tpar(i) = tpar(i)+temp2(nd,jlev)*arco(j)
                   spar(i) = spar(i)+salt2(nd,jlev)*arco(j)
                   mizopar(i)=mizopar(i)+mizoo2(nd,jlev)*arco(j)
                   mezopar(i)=mezopar(i)+mezoo2(nd,jlev)*arco(j)
                   oxypar(i)=oxypar(i)+oxy2(nd,jlev)*arco(j)
                   extpar(i)=extpar(i)+ext2(nd,jlev)*arco(j)
                   preypar(i) = preypar(i)+prey2(nd,jlev)*arco(j)
                   detripar(i) = detripar(i)+detri2(nd,jlev)*arco(j)
                   dompar(i) = dompar(i)+dom2(nd,jlev)*arco(j)
                   if(istage(i) == 1) then !eggs are stationary
                     upar(i)=0
                     vpar(i)=0
                     wpar(i)=0
                   endif !istage
                   if(mod_part==1) then 
                     wndx(i) = wndx(i)+wnx2(nd)*arco(j)
                     wndy(i) = wndy(i)+wny2(nd)*arco(j)
                   endif !mod_part
                 enddo !j
               endif !wet
             endif !levpari
             print*,'before wetting and drying'
             !Wetting and drying
             print*,'ielpar(i)',ielpar(i) 
             if(idry_e(ielpar(i))==1)then 
               levpar(i) =-1
               drycount(i) = drycount(i)+1.0;
               print*,'dry particles cant survive',i
               go to 449
             endif
          if(istage(i).lt.4) then 
          nnel=ielpar(i)
          print*,'i,ielpar',i,ielpar(i),levpar(i)
          jlev=levpar(i)
          !rewetted elements
          if(jlev==-1) then  !element nnel wet changed :) 
            jlev=nvrt
            drycount(i)=0.0
            zpar(i) = sum(eta3(elnode(1:i34(nnel),nnel)))/i34(nnel) 
          endif
            !Tracking
            x0=xpar(i)
            y0=ypar(i)
            z0=zpar(i)
            
            !umwandeln von zpar in meter
            nnel0 = ielpar(i)
            jlev0 = jlev
            !changeing ndeltp based on the life stage 
            !to decrease the computational time as no 
            !tracking is needed in the first two stages 
            if(istage(i)<3) then 
              ndeltp=1
            elseif(istage(i)==3) then 
              ndeltp=ndeltp_ini
            endif 
            dtb=pt/ndeltp
            do idt=1,ndeltp
              if(ibf==1) then 
                trat=real(idt)/ndeltp
            else
                trat=real(idt-1)/ndeltp
            endif
            if(mod_part==1) then !oilspill
                !wind rotation 
                dir=atan2(wndy(i),wndx(i))
                speed = sqrt(wndx(i)**2+wndy(i)**2)
                dir=dir+rotate_angle
                if(iwind==0 ) then 
                  cur_x=0;cur_y=0
                else 
                  cur_x=speed*sin(dir)*drag_c
                  cur_y=speed*cos(dir)*drag_c
                endif 
                if(z0<-0.1) then 
                  cur_x=0;cur_y=0 !sub-surf part not influenced by wind
                endif 
               !generating random number 
                do k=1,3
                  dx(k) = ran1(iseed)
                  dy(k) = ran2(iseed) 
                  dz(k) = ran3(iseed)
                enddo!k
                rndx=dx(1)
                rndy=dx(2)
                rndz=dx(3)
                if(istage(i).lt.2) then 
                  upar(i)=0.0
                  vpar(i)=0.0
                  wpar(i)=0.0
                endif!istage
                xadv=(upar(i)+cur_x+grdx(i))*dtb
                yadv=(vpar(i)+cur_y+grdy(i))*dtb
                if(retention>=1) then 
                  !implementing the effect of tidal retention 
                  if(retention==1) then !going with the tidal flow 
                  if(etaap(i)>0.0) then 
                     ret_dir = 1
                  elseif(etaap(i)<0.0) then 
                     ret_dir = -1
                  endif 
                  elseif(retention==2) then
                  if(etaap(i)>0.0) then 
                     ret_dir = -1
                  elseif(etaap(i)<0.0) then 
                     ret_dir = 1
                  endif 
                  endif  
                  ret_speed = 0.5*((lengthpar2(i)*5.)/1000.)
                  rsl = ((swspd(i))/1000.)*ret_dir

                  print*,'retention debug'
                  print*,'retention=',retention
                  print*,'ret_dir',ret_dir,'etap',etaap(i)
                  print*,'ret_speed',rsl,lengthpar2(i),swspd(i)
                 endif !retention

                zadv=(wpar(i)+rsl+grdz(i))*dtb
                print*,'zadv',wpar(i),rsl,rsl_ref,grdz(i),dtb,zadv
               
                xdif=(2*rndx-1)*sqrt(6*dhfx(i)*dtb) !random walt
                ydif=(2*rndy-1)*sqrt(6*dhfy(i)*dtb)
                zdif=(2*rndz-1)*sqrt(6*dhfz(i)*dtb)
                if(istage(i)<=2) then 
                  xadv=0.0
                  yadv = 0.0
                  zadv = 0.0
                  xdif = 0.0
                  ydif = 0.0
                  zdif = 0.0
                endif
                xt=x0+xadv+xdif
                yt=y0+yadv+ydif
                zt=z0+zadv+zdif
                print*,'z speed',z0,zadv,zdif
              else !not mod_part=1
                xt=x0+ibf*dtb*upar(i)
                yt=y0+ibf*dtb*vpar(i)
                zt=z0+ibf*dtb*wpar(i)
                rnds=0 ! not used
              endif !mod_part=1
              print*,'nnel0 before qs',nnel0
              call quicksearch(1,idt,i,nnel0,jlev0,dtb,x0,y0,z0,xt,yt,zt,nnel,jlev, &
              &node12,arco,zrat,nfl,eta_p,dp_p,ztmp,kbpl,rnds,pbeach,inflag,&
              &parstat,maxnpar)
              print*,'made quicksearch'
        
              ! adding values for diagnostics 
              etaap(i) = eta_p
              kbbpp(i) = kbpl
              print*, 'after diagnostics'
              do j=1,i34(nnel)
                nd=elnode(j,nnel)
                if(mod_part==1) then 
                  vwx(j)=wnx1(nd)*(1-trat)+wnx2(nd)*trat !windx
                  vwy(j)=wny1(nd)*(1-trat)+wny2(nd)*trat
                endif !mod_part
                do l=1,2
                  lev=jlev+l-2
                  vxl(j,l)=uu1(nd,lev)*(1-trat)+uu2(nd,lev)*trat
                  vyl(j,l)=vv1(nd,lev)*(1-trat)+vv2(nd,lev)*trat
                  vzl(j,l)=ww1(nd,lev)*(1-trat)+ww2(nd,lev)*trat
                  vmizol(j,l)=mizoo1(nd,lev)*(1-trat)+mizoo2(nd,lev)*trat
                  vmezol(j,l)=mezoo1(nd,lev)*(1-trat)+mezoo2(nd,lev)*trat
                  voxyl(j,l)=oxy1(nd,lev)*(1-trat)+oxy2(nd,lev)*trat
                  templ(j,l)=temp1(nd,lev)*(1-trat)+temp2(nd,lev)*trat
                  vdetrl(j,l)=detri1(nd,lev)*(1-trat)+detri2(nd,lev)*trat
                  vdoml(j,l)=dom1(nd,lev)*(1-trat)+dom2(nd,lev)*trat
                  saltl(j,l)=salt1(nd,lev)*(1-trat)+salt2(nd,lev)*trat
                  extl(j,l)=ext1(nd,lev)*(1-trat)+ext2(nd,lev)*trat
                  preyl(j,l)=prey1(nd,lev)*(1-trat)+prey2(nd,lev)*trat
                  if(mod_part==1) then
                    val(j,l)=hf1(nd,lev)*(1-trat)+hf2(nd,lev)*trat
                    vbl(j,l)=vf1(nd,lev)*(1-trat)+vf2(nd,lev)*trat
                    vcl(j,l)=hf1(nd,lev)*(1-trat/2)+hf2(nd,lev)*trat/2
                    vdl(j,l)=vf1(nd,lev)*(1-trat/2)+vf2(nd,lev)*trat/2
                  endif!mod_part
                enddo !l
              enddo !j
              !interpolate in vertical 
              do j=1,i34(nnel)
                vxn(j)=vxl(j,2)*(1-zrat)+vxl(j,1)*zrat
                vyn(j)=vyl(j,2)*(1-zrat)+vyl(j,1)*zrat
                vzn(j)=vzl(j,2)*(1-zrat)+vzl(j,1)*zrat
                mizon(j)=vmizol(j,2)*(1-zrat)+vmizol(j,1)*zrat
                mezon(j)=vmezol(j,2)*(1-zrat)+vmezol(j,1)*zrat
                oxyn(j)=voxyl(j,2)*(1-zrat)+voxyl(j,1)*zrat
                detrin(j) = vdetrl(j,1)*(1-zrat)+vdetrl(j,1)*zrat
                domn(j) = vdoml(j,1)*(1-zrat)+vdoml(j,1)*zrat
                tempn(j)=templ(j,2)*(1-zrat)+templ(j,1)*zrat
                saltn(j)=saltl(j,2)*(1-zrat)+saltl(j,1)*zrat
                extn(j)=extl(j,2)*(1-zrat)+extl(j,1)*zrat
                preyn(j)=preyl(j,2)*(1-zrat)+preyl(j,1)*zrat
                if(mod_part==1) then 
                  van(j)=val(j,2)*(1-zrat)+val(j,1)*zrat
                  vcn(j)=vcl(j,2)*(1-zrat)+vcl(j,1)*zrat
                  vdn(j)=vdl(j,2)*(1-zrat)+vdl(j,1)*zrat
                endif !mod_parti
              enddo !j

              !interpolate in horizontal 
              upar(i)=0;vpar(i)=0;wpar(i)=0
              wndx(i)=0;wndy(i)=0;dhfx(i)=0;dhfz(i)=0
              tpar(i)=0;spar(i)=0
              mizopar(i)=0; mezopar(i)=0
              oxypar(i)=0
              extpar(i) = 0
              preypar(i) = 0
              dompar(i)=0
              detripar(i) = 0
              !correction preparation for tracers
              if (minval(tempn(node12))==0.0) then
                  print*,'correction of tracers'
                  print*,'tempn',tempn(node12),mizon(node12)
                  print*,'oxyn',oxyn(node12)
                  do j=1,3
                  id = node12(j)
                     if(tempn(id)==0.0) then 
                        tempn(id) = maxval(tempn(node12))
                        extn(id) = maxval(extn(node12))
                        saltn(id) = maxval(saltn(node12))
                        oxyn(id) = maxval(oxyn(node12))
                     endif 
                  end do 
                  print*,'after correction'
                  print*,tempn(node12),mizon(node12)
                  print*,oxyn(node12)
               endif 
                  
                
              do j=1,3
                id=node12(j)
                ! check if id is dry
                upar(i)=upar(i)+vxn(id)*arco(j)
                vpar(i)=vpar(i)+vyn(id)*arco(j)
                wpar(i)=wpar(i)+vzn(id)*arco(j)
                mizopar(i)=mizopar(i)+mizon(id)*arco(j)
                mezopar(i)=mezopar(i)+mezon(id)*arco(j)
                oxypar(i) = oxypar(i)+oxyn(id)*arco(j)
                tpar(i)=tpar(i)+tempn(id)*arco(j)
                spar(i)=spar(i)+saltn(id)*arco(j)
                extpar(i)=extpar(i)+extn(id)*arco(j)
                preypar(i) = preypar(i)+preyn(id)*arco(j)
                detripar(i) = detripar(i)+detrin(id)*arco(j)
                dompar(i) = dompar(i)+domn(id)*arco(j)
                if(mod_part==1) then 
                  wndx(i)=wndx(i)+vwx(id)*arco(j)
                  wndy(i)=wndy(i)+vwy(id)*arco(j)
                  dhfx(i)=dhfx(i)+vcn(id)*arco(j)
                  dhfz(i)=dhfz(i)+vdn(id)*arco(j)
                endif
              enddo !j
              print*,'dhfx',dhfx(i),upar(i)
              dhfy(i)= dhfx(i)
              !compute the vert grad of diffusion 
              if(mod_part==1) then 
                az=0
                do j=1,3
                  id=node12(j)
                  az=az+(vbl(id,2)-vbl(id,1))*arco(j)
                enddo
                tmp=ztmp(jlev)-ztmp(jlev-1)
                if(tmp==0) then 
                  grdz(i)=0
                else 
                  grdz(i)=az/tmp
                endif
                !gradient of viscosity 
                grdx(i)=dot_product(van(node12(1:3)),dldxy(1:3,1,nnel))
                grdy(i)=dot_product(van(node12(1:3)),dldxy(1:3,2,nnel))
              endif
              if(nfl==1) then 
                iabnorm(i)=1
                print*,'idry_e abnorm',idry_e(nnel)
                exit
              endif 
              x0=xt
              y0=yt
              z0=zt
              nnel0=nnel
              jlev0=jlev    
            enddo ! idt=1,ndeltp
            xpar(i)=xt
            ypar(i)=yt
            zpar(i)=zt
            ielpar(i)=nnel
            levpar(i)=jlev
           
           elseif(istage(i)==4) then 
            print*,'istage =4, tracking skipped for',i
            parstat(i)=0
           endif
           endif
449        continue
           print*,'continued after 449'           
            if(mod_part==1) then  
              if(ist(i)/=0) then 
                y0=1!initial mass 
                yc=y0*remain_ratio
                arg=-log(2.0)/T_half*(time-st_p(i))
                amas(i)=yc+(y0-yc)*exp(max(arg,-20.d0))
              endif 
            endif !modpart
          if(ics==2) then 
            call cppinverse(xout,yout,xpar(i),ypar(i),slam0,sfea0)
            xout=xout*180.0/pi
            yout=yout*180.0/pi
          else
            xout=xpar(i)
            yout=ypar(i)
          endif
          print*,'calculations of the IBM depending on istage'
          if(smibm==1 .and. parstat(i)==1) then
            print*,'istage(i) = ',istage(i)
            if(istage(i).lt.2) then
            print*,'egg calculation starts'
            !smelt egg development
            call smelt_egg(i,maxnpar,nstart,tpar(i),spar(i),EggDev1,EggDev2,N_indi,EggHatchL(i))
            elseif(istage(i)==2) then
              print*,'yolk sac called',time
              call smelt_yolksac(i,maxnpar,tpar(i),YolkDev1,YolkDev2,N_indi,lengthpar1(i),weightpar1(i),lifeq_fac)
              print*,'yolk sac successfull'
            elseif(istage(i)==3) then
                 print*,'swsp before',swspd(i)
                 call smelt_larvae(i,itrb,maxnpar,tpar(i),trbpar(i),spar(i),extpar(i),oxypar(i),lengthpar1(i),& 
                 &lengthpar2(i),weightpar1(i),weightpar2(i),swspd(i),metabr(i),lifeq(i),&
                 &lifeq_fac,fpr,plx,wpr,preypar(i),iipr)
                 print*,'swsp after',swspd(i)
            elseif(istage(i)==4) then
               parstat(i)=0
               print*,'particle dies as istage=4' 
            endif
            !call the mortality subroutine 
            call ibm_mortality(istage(i),N_indi(i),tpar(i),oxypar(i),weightpar2(i),lifeq(i),extpar(i)) 
            if(N_indi(i).le.N_lim) then !if superindividual dies, then the particle dies aswell
              parstat(i) = 0
              istage(i) = 4
              pdead = 1;
              print*,'particle',i,'died of N_lim'
            endif
          endif
          !end if!parstat check
          enddo!maxnpar
        !write for the next step
        uu1=uu2;vv1=vv2;ww1=ww2;eta1=eta2
        temp1=temp2;salt1=salt2
        hf1=hf2;vf1=vf2;wnx1=wnx2;wny1=wny2
        flage1=flage2;diat1=diat2;mizoo1=mizoo2;mezoo1=mezoo2
        detri1=detri2;nh41=nh42;no31=no32;po41=po42;oxy1=oxy2
        ext1=ext2;prey1=prey2;dom1=dom2
        if(smibm==1) then
          EggDev2=EggDev1
          YolkDev2 = YolkDev1
          weightpar2 = weightpar1
          lengthpar2 = lengthpar1
        endif

        call partstat_check_int(maxnpar,parstat,istage,levpar,nnes)
        call partstat_check_vector(maxnpar,parstat,xpar,nnes)
        call partstat_check_vector(maxnpar,parstat,ypar,nnes) 
        call partstat_check_vector(maxnpar,parstat,zpar,nnes)
        call partstat_check_vector(maxnpar,parstat,tpar,nnes) 
        call partstat_check_vector(maxnpar,parstat,spar,nnes)
        call partstat_check_vector(maxnpar,parstat,upar,nnes) 
        call partstat_check_vector(maxnpar,parstat,vpar,nnes) 
        call partstat_check_vector(maxnpar,parstat,wpar,nnes)
        call partstat_check_vector(maxnpar,parstat,swspd,nnes)
        call partstat_check_vector(maxnpar,parstat,preypar,nnes)
        call partstat_check_vector(maxnpar,parstat,extpar,nnes)
        call partstat_check_vector(maxnpar,parstat,EggDev1,nnes)
        call partstat_check_vector(maxnpar,parstat,EggDev2,nnes)
        call partstat_check_vector(maxnpar,parstat,YolkDev1,nnes)
        call partstat_check_vector(maxnpar,parstat,YolkDev2,nnes)
        call partstat_check_vector(maxnpar,parstat,EggHatchL,nnes)
        call partstat_check_vector(maxnpar,parstat,weightpar1,nnes)
        call partstat_check_vector(maxnpar,parstat,weightpar2,nnes)
        call partstat_check_vector(maxnpar,parstat,lengthpar1,nnes)
        call partstat_check_vector(maxnpar,parstat,lengthpar2,nnes)
        call partstat_check_vector(maxnpar,parstat,oxypar,nnes)
        call partstat_check_vector(maxnpar,parstat,mizopar,nnes)
        call partstat_check_vector(maxnpar,parstat,mezopar,nnes)
        call partstat_check_vector(maxnpar,parstat,trbpar,nnes)
        call partstat_check_vector(maxnpar,parstat,etaap,nnes)
        call partstat_check_vector(maxnpar,parstat,kbbpp,nnes)
        call partstat_check_vector(maxnpar,parstat,grdx,nnes)
        call partstat_check_vector(maxnpar,parstat,grdy,nnes)
        call partstat_check_vector(maxnpar,parstat,dhfx,nnes)
        call partstat_check_vector(maxnpar,parstat,dhfy,nnes)
        call partstat_check_vector(maxnpar,parstat,EggHatchL,nnes)
        call partstat_check_vector_writeout(time,maxnpar,parstat,idpar,ielpar,nnes)
        if(timing==1) then 
          call system_clock(count,count_rate)
        endif
       if(time>0.0) then
          call netcdf_output(idpar(:),xpar(:),ypar(:),zpar(:),tpar(:),spar(:),upar(:),&
          &vpar(:),wpar(:),grdx(:),grdy(:),dhfx(:),mizopar(:),mezopar(:),trbpar(:),oxypar(:),preypar(:),drycount(:), &
          &istage(:),N_indi(:),extpar(:),ielpar(:),levpar(:),etaap(:),kbbpp(:),iths,it1,maxnpar,time,ihot,ihot_start)
          call write_netcdf_output_ibm(time,maxnpar,iths,it1,EggDev2(:),EggHatchL(:),YolkDev2(:),weightpar2(:), &
        & lengthpar2(:),swspd(:),lifeq(:),metabr(:),ihot,ihot_start)
        endif
        if(nscreen.eq.1) write(*,*)'Time(days)=',time/86400,'it=',it1,it2
      enddo!it
      print*,'is end end',it1,it2
      stop
      end  
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
!                       Subroutines                                                   
!
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

! ****************************************************      
! ****************************************************      
! RANDOM NUMBER GENERATORS from numerical recipes       
! ran[1-3] are from 3 methods
! ****************************************************
  FUNCTION ran1(idum)
   Integer idum,IM1,IM2,IMM1,IA1,IA2,IQ1,IQ2,IR1,&
      & IR2,NTAB,NDIV
   Real*8 ran1,AM,EPS,RNMX
   Parameter(IM1=2147483563,IM2=2147483399,AM=1./&
      & IM1,IMM1=IM1-1,IA1=40014,IA2=40692,IQ1=53668,&
      & IQ2=52774,IR1=12211,IR2=3791,NTAB=32,NDIV=1+&
      & IMM1/NTAB,EPS=1.2e-7,RNMX=1.-EPS)
   Integer idum2,j,k,iv(NTAB),iy
   SAVE iv,iy,idum2
   DATA idum2/123456789/,iv/NTAB*0/,iy/0/
! ....................................................
   if(idum.le.0) then
     idum=max(-idum,1)
     idum2=idum
     do j=NTAB+8,1,-1
        k=idum/IQ1
        idum=IA1*(idum-k*IQ1)-k*IR1
        if(idum.lt.0) idum=idum+IM1
        if(j.le.NTAB) iv(j)=idum
     enddo
     iy=iv(1) 
   endif
   k=idum/IQ1
   idum=IA1*(idum-k*IQ1)-k*IR1
   if(idum.lt.0) idum=idum+IM1
     k=idum2/IQ2
     idum2=IA2*(idum2-k*IQ2)-k*IR2
   if(idum2.lt.0) idum2=idum2+IM2
     j=1+iy/NDIV
     iy=iv(j)-idum2
     iv(j)=idum
   if(iy.lt.1) iy=iy+IMM1
     ran1=min(AM*iy,RNMX)  
   return
  end

  FUNCTION ran2(idum)
   Integer idum,IM1,IM2,IMM1,IA1,IA2,IQ1,IQ2,IR1,&
        & IR2,NTAB,NDIV
   Real*8 ran2,AM,EPS,RNMX
   Parameter(IM1=2147483563,IM2=2147483399,AM=1./&
& IM1,IMM1=IM1-1,IA1=40014,IA2=40692,IQ1=53668,&
& IQ2=52774,IR1=12211,IR2=3791,NTAB=32,NDIV=1+&
& IMM1/NTAB,EPS=1.2e-7,RNMX=1.-EPS)
   Integer idum2,j,k,iv(NTAB),iy
   SAVE iv,iy,idum2
   DATA idum2/123456789/,iv/NTAB*0/,iy/0/
! ....................................................
   if(idum.le.0) then
     idum=max(-idum,1)
     idum2=idum
     do j=NTAB+8,1,-1
k=idum/IQ1
idum=IA1*(idum-k*IQ1)-k*IR1
if(idum.lt.0) idum=idum+IM1
if(j.le.NTAB) iv(j)=idum
     enddo
     iy=iv(1) 
   endif
   k=idum/IQ1
   idum=IA1*(idum-k*IQ1)-k*IR1
   if(idum.lt.0) idum=idum+IM1
     k=idum2/IQ2
     idum2=IA2*(idum2-k*IQ2)-k*IR2
   if(idum2.lt.0) idum2=idum2+IM2
     j=1+iy/NDIV
     iy=iv(j)-idum2
     iv(j)=idum
   if(iy.lt.1) iy=iy+IMM1
     ran2=min(AM*iy,RNMX)  
   return
  end

  FUNCTION ran3(idum)
   Integer idum
   Integer MBIG,MSEED,MZ
   Real*8 ran3,FAC 
   Parameter(MBIG=1000000000,MSEED=161803398,MZ=0,FAC=1./MBIG)
   Integer i,iff,ii,inext,inextp,k
   Integer mj,mk,ma(55) 
   SAVE iff,inext,inextp,ma
   DATA iff /0/
! ........................................................... 
   if(idum.lt.0.or.iff.eq.0) then
     iff=1
     mj=abs(MSEED-abs(idum))
     mj=mod(mj,MBIG)
     ma(55)=mj
     mk=1
     do i=1,54
ii=mod(21*i,55)
ma(ii)=mk
mk=mj-mk
if(mk.lt.MZ) mk=mk+MBIG
mj=ma(ii)
     enddo 
     do k=1,4
do i=1,55
ma(i)=ma(i)-ma(1+mod(i+30,55))
if(ma(i).lt.MZ) ma(i)=ma(i)+MBIG
enddo
     enddo
     inext=0
     inextp=31
     idum=1
    endif
     inext=inext+1
     if(inext.eq.56) inext=1
inextp=inextp+1
     if(inextp.eq.56) inextp=1
mj=ma(inext)-ma(inextp)
     if(mj.lt.MZ) mj=mj+MBIG
ma(inext)=mj
ran3=mj*FAC
   return
   end 

!***********************************************************
!   Transform from lon,lat coordinates into CPP coordinates! 
!   Lon and Lat need to be in radians                      !
!***********************************************************

subroutine cpp(x,y,rlambda,phi,rlambda0,phi0)
use kind_par
implicit real(kind=dbl_kind1)(a-h,o-z), integer(i-n)

r=6378206.4
x=r*(rlambda-rlambda0)*cos(phi0)
y=phi*r

return
end 

subroutine cppinverse(rlambda,phi,x,y,rlambda0,phi0)
use kind_par
implicit real(kind=dbl_kind1)(a-h,o-z),integer(i-n)

r=6278206.4
rlambda=x / (r*cos(phi0))+rlambda0
phi=y/r
return 
end 

!
!********************************************************************************
!										*
!     Program to detect if two segments (1,2) and (3,4) have common pts   	*
!     Assumption: the 4 pts are distinctive.					*
!     The eqs. for the 2 lines are: X=X1+(X2-X1)*tt1 and X=X3+(X4-X3)*tt2.	*
!     Output: iflag: 0: nointersection.	*
!     If iflag=1, (xin,yin) is the intersection.				*
!										*
!********************************************************************************
!

subroutine intersect2(x1,x2,x3,x4,y1,y2,y3,y4,iflag,xin,yin,tt1,tt2)
use kind_par
implicit real(kind=dbl_kind1)(a-h,o-z), integer(i-n)
real(kind=dbl_kind1), parameter :: zero1=0.0 !small positive number or 0

real(kind=dbl_kind1), intent(in) :: x1,x2,x3,x4,y1,y2,y3,y4
integer, intent(out) :: iflag
real(kind=dbl_kind1), intent(out) :: xin,yin,tt1,tt2

tt1=-1000
tt2=-1000
iflag=0
delta=(x2-x1)*(y3-y4)-(y2-y1)*(x3-x4)
delta1=(x3-x1)*(y3-y4)-(y3-y1)*(x3-x4)
delta2=(x2-x1)*(y3-y1)-(y2-y1)*(x3-x1)
if(delta.ne.0.0d0) then
tt1=delta1/delta
tt2=delta2/delta
if(tt1.ge.-zero1.and.tt1.le.1+zero1.and.tt2.ge.-zero1.and.tt2.le.1+zero1) then
iflag=1
xin=x1+(x2-x1)*tt1
yin=y1+(y2-y1)*tt1
        endif
endif

        return
        end



        function signa(x1,x2,x3,y1,y2,y3)
        use kind_par
        implicit real(kind=dbl_kind1)(a-h,o-z),integer(i-n)

        signa=((x1-x3)*(y2-y3)-(x2-x3)*(y1-y3))/2

        return
        end 
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                                                                       !
!       Compute area coordinates of pt (xt,yt), which must be inside element nnel.      !
!       Impose bounds for area coordinates.                                             !
!                                                                                       !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      subroutine area_coord(nnel,xt,yt,arco)
      use globlib, only : dbl_kind,elnode,area,x,y
      implicit real(kind=dbl_kind)(a-h,o-z),integer(i-n)
      integer, intent(in) :: nnel
      real(kind=dbl_kind), intent(in) :: xt,yt
      real(kind=dbl_kind), intent(out) :: arco(3)

      n1=elnode(1,nnel)
      n2=elnode(2,nnel)
      n3=elnode(3,nnel)
      arco(1)=signa(xt,x(n2),x(n3),yt,y(n2),y(n3))/area(nnel)
      arco(2)=signa(x(n1),xt,x(n3),y(n1),yt,y(n3))/area(nnel)
      arco(1)=max(0.0d0,min(1.0d0,arco(1)))
      arco(2)=max(0.0d0,min(1.0d0,arco(2)))
      if(arco(1)+arco(2)>1) then 
        arco(3)=0
        arco(1)=min(1.d0,max(0.0,arco(1)))
        arco(2)=1-arco(1)
      else 
        arco(3)=1-arco(1)-arco(2)
      endif

      return
      end

!
!********************************************************************
!								    *
!	Routine to update z-coordinates and wetting and drying      *
!								    *
!********************************************************************
!
      subroutine levels
      use globlib
      use compute_zcor
      implicit real(kind=dbl_kind)(a-h,o-z),integer(i-n)
      dimension idry_new(np) !,out2(12+mnv)
      real(dbl_kind) :: zlcl(nvrt)
!...  z-coor. for nodes
!...  
      do i=1,np
        if(dp(i)+eta3(i)<=h0) then !dry
          idry_new(i)=1 
          if(ivcor==2) then; if(dp(i)>=h_s) then
            write(11,*)'Deep depth dry:',i,h_s,dp(i)
            stop
          endif; endif
!          kbp(i)=0
        else !wet
          idry_new(i)=0
          if(ivcor==2) then
            call zcor_SZ_double(dp(i),eta3(i),h0,h_s,h_c,theta_b, &
            &theta_f,kz,nvrt,ztot,sigma,zlcl,idry_tmp,kbpl)
            if(idry_tmp==1) then
            write(11,*)'Impossible dry (7):',i,idry_tmp,dp(i),eta1(i),eta2(i),eta3(i),kbpl
            stop
           endif
           !Cannot use kbp00 b/cos wet/dry
           kbp(i)=kbpl
           z(i,kbpl:nvrt)=zlcl(kbp(i):nvrt)
           else if(ivcor==1) then
           z(i,kbp(i):nvrt)=(eta3(i)+dp(i))*sigma_lcl(kbp(i):nvrt,i)+eta3(i)
           else
             write(11,*)'Unknown ivcor:',ivcor
             stop
           endif

           do k=kbp(i)+1,nvrt
              if(z(i,k)-z(i,k-1)<=0) then
              print*,'kbp(i)',kbp(i),z(i,:)
              print*,'---------'
              print*,'eta3(i),dp(i)',eta3(i),dp(i)
              print*,'sigma',sigma_lcl(:,i)
              write(11,*)'Inverted z-levels at:',i,k,z(i,k)-z(i,k-1),eta3(i)
              stop
              endif
            enddo !k
         endif 
       enddo !1:np

!... Set wet/dry flags for element; elemnt is dry if one of the nodes is dry; conversly,
!... an element is wet if all nodes are wet (including all sides) 
!... Weed out fake nodes; a node is wet if and only if at least one surrounding elem is wet 
!...
!       idry_e0=idry_e !save
       idry=1
       kbe=0
       do i=1,ne
          idry_e(i)=maxval(idry_new(elnode(1:i34(i),i)))
          if(idry_e(i)==0) then 
            idry(elnode(1:i34(i),i))=0
            kbe(i)=minval(kbp(elnode(1:i34(i),i)))
          endif
       enddo !i
       return 
       end 
!
!********************************************************************************
!	
!     Straightline search algorithm. Initially nnel0 is an element that 	
!     encompasses (x0,y0). iloc=0: do not nudge initial pt; iloc=1: nudge.	 
!     Input: iloc,idt,ipar,nnel0,x0,y0,z0,xt,yt,zt,jlev0, time, and uu2,vv2,ww2 for 	
!	abnormal cases;								
!     Output the updated end pt (xt,yt,zt) (if so), nnel1, jlev1, area          
!       coordinates, vertical ratio, a flag nfl, and local elevation and depth. 
!       I/O for oil spill: ist2,inbr2,rnds,pbeach
!     nfl=1 if a bnd or dry element is hit and vel. there is small,		 
!	or death trap is reached.						
!										
!********************************************************************************
!
     subroutine quicksearch(iloc,idt,ipar,nnel0,jlev0,time,x0,y0,z0,xt,yt,zt,nnel1,jlev1, &
     &nodel2,arco,zrat,nfl,etal,dp_p,ztmp,kbpl,rnds,pbeach,inflag,parstat,&
     &maxnpar)

      use globlib
      use compute_zcor
      implicit real(kind=dbl_kind)(a-h,o-z),integer(i-n)
      integer, intent(in) :: iloc,idt,ipar,nnel0,jlev0,maxnpar
      integer, intent(out) :: inflag
      integer, intent(inout) :: parstat(maxnpar)
      real(kind=dbl_kind), intent(in) :: time,x0,y0,z0,rnds,pbeach
      integer, intent(out) :: nnel1,jlev1,nodel2(3),nfl,kbpl
      !integer, intent(out) :: nnel1,jlev1,nodel2(3),nfl,kbpl,ist2,inbr2
      real(kind=dbl_kind), intent(inout) :: xt,yt,zt
      real(kind=dbl_kind), intent(out) :: arco(3),zrat,etal,dp_p,ztmp(nvrt)
      !Local
      real(dbl_kind) :: zlcl(nvrt)
      logical ::  ltmp1,ltmp2

      if(iloc>1) then
         write(11,*)'iloc > 1'
         stop
      endif
      if(idry_e(nnel0)==1) then
         print*,'Starting element is dry'
         parstat(ipar) = 0
         goto 421
      endif

      nfl=0
      trm=time !time remaining

!     Starting element nel
      nel=nnel0
!     An interior pt close to (x0,y0) to prevent underflow for iloc >=1.
      if(iloc==0) then
         xcg=x0
         ycg=y0
      else if(iloc==1) then
         xcg=(1-1.0d-4)*x0+1.0d-4*xctr(nel)
         !print*,xctr(nel)
         ycg=(1-1.0d-4)*y0+1.0d-4*yctr(nel)
      endif
      print*,'pt_in_poly',x(elnode(1:i34(nel),nel)),xcg
      call pt_in_poly2(i34(nel),x(elnode(1:i34(nel),nel)),y(elnode(1:i34(nel),nel)),xcg,ycg,inside)
!      aa=0
!      aa1=0
!      do i=1,3
!        n1=elnode(i,nel)
!        n2=elnode(nxq(1,i,i34(nel)),nel)
!        aa=aa+dabs(signa(x(n1),x(n2),x0,y(n1),y(n2),y0))
!        aa1=aa1+dabs(signa(x(n1),x(n2),xt,y(n1),y(n2),yt))
!      enddo !i
!      ae=dabs(aa-area(nel))/area(nel)
!      if(ae>small1) then
      if(inside==0) then
         print*,'xo',x0,y0
         print*,'xcg',xcg,ycg
         print*,'xt',xt,yt
         print*,'(x0,y0) not in nnel0 initially',time,ipar,nnel0,xcg,ycg
         parstat(ipar) = 0
         stop
         go to 421
      endif
      call pt_in_poly2(i34(nel),x(elnode(1:i34(nel),nel)),y(elnode(1:i34(nel),nel)),xt,yt,inside)
!      ae=dabs(aa1-area(nel))/area(nel)
!      if(ae<small1) then
      if(inside/=0) then
         nnel1=nel
         go to 400
      endif

!     (xt,yt) not in nel, and thus (xcg,ycg) and (xt,yt) are distinctive
      pathl=dsqrt((xt-xcg)**2+(yt-ycg)**2)
      if(pathl==0) then
         write(11,*)'Zero path',x0,y0,xt,yt,xcg,ycg
         stop
      endif

!     Starting edge nel_j
      nel_j=0
      do j=1,i34(nel)
         jd1=elnode(nxq(1,j,i34(nel)),nel)
         jd2=elnode(nxq(2,j,i34(nel)),nel)
         call intersect2(xcg,xt,x(jd1),x(jd2),ycg,yt,y(jd1),y(jd2),iflag,xin,yin,tt1,tt2)
         if(iflag==1) then
            inflag=0
            nel_j=j
            exit
         endif
      enddo !j=1,3
      if(nel_j==0) then
         inflag=1
         print*,'Found no intersecting edges I:',nel,xcg,ycg,xt,yt
         stop
         !return
      endif
      zin=z0 !intialize
      itt=0
      loop4: do
!----------------------------------------------------------------------------------------
      itt=itt+1
      if(itt>1000) then
         if(ifort12(3)==0) then
            ifort12(3)=1
            write(12,*)'Death trap reached',idt
            inflag=1
         endif
         nfl=1
         xt=xin
         yt=yin
         zt=zin
         nnel1=nel
         exit loop4
      endif
      md1=elnode(nxq(1,nel_j,i34(nel)),nel)
      md2=elnode(nxq(2,nel_j,i34(nel)),nel)

!     Compute z position 
      dist=sqrt((xin-xt)**2+(yin-yt)**2)
!      if(dist/pathl.gt.1+1.0d-4) then
!        write(11,*)'Path overshot'
!        stop
!      endif
      tmp=min(1.d0,dist/pathl)
      zin=zt-tmp*(zt-zin)
      trm=trm*dist/pathl !time remaining
      pathl=sqrt((xin-xt)**2+(yin-yt)**2)
!     print*,'pathl,trm',pathl,trm
      if(pathl==0.or.trm==0) then
         write(11,*)'Target reached'
         stop
      endif

      lit=0 !flag
!     For horizontal exit and dry elements
      if(ic3(nel_j,nel)==0.or.idry_e(max(1,ic3(nel_j,nel)))==1) then
         lit=1
         !print*,'lit=1'
         isd=elside(nel_j,nel)
         if(isidenode(1,isd)+isidenode(2,isd)/=md1+md2) then
            write(11,*)'Wrong side'
            stop
         endif

         !Oil spill
         !if(mod_part==1) then
         !Set status flag
           ! if(ic3(nel_j,nel)==0) then
           !    ltmp1=isbnd(md1)>0.or.isbnd(md2)>0 !open bnd
           !    ltmp2=isbnd(md1)==-1.and.isbnd(md2)==-1 !permanently stranded @ land bnd
            !   print*,'ltmp12',ltmp1,ltmp2,isbnd(md1),isbnd(md2)
            !    if(ltmp1.or.ltmp2) then
            !      if(ltmp1) then
            !         ist2=2
            !         print*,'ist2=2'
            !      else
            !         ist2=2 !permanently stranded
            !         print*,'ist2=-1'
            !      endif
            !      print*,'mds'
            !      print*,md1
            !      print*,'-------'
            !      print*,md2
            !      print*,'------'
            !      print*,nxq(1,:,3)
            !      print*,nxq(2,:,3)
            !      print*,'-------'
            !      print*,elnode(nxq(1,:,3),nel)
            !      print*,elnode(nxq(2,:,3),nel)
            !      print*,'------'
            !      print*,isbnd(elnode(nxq(1,:,3),nel))
            !      print*,isbnd(elnode(nxq(2,:,3),nel))
            !      nfl=1
            !      xt=xin
            !      yt=yin
            !      zt=zin
            !      nnel1=nel
            !      exit loop4
            !   endif
            !else !internal side with a dry elem.
            !print*,'internal side with a dry elem'
            !print*,ic3(nel_j,nel),nel,nel_j
            !print*,ic3(:,nel)
            !print*,md1,md2
            !print*,'--------------'
            !print*,idry_e(max(1,ic3(nel_j,nel)))
            !print*,'idy_e',idry_e(ic3(:,nel))
            !print*,'nxq',nxq(1,nel_j,i34(nel)),nxq(2,nel_j,i34(nel)),nxq(3,nel_j,i34(nel))
            !print*,shape(nxq)
            !print*,'----------'
            !print*,nxq(1,:,i34(nel))
            !print*,nxq(2,:,i34(nel))
         !      if(rnds>=pbeach) then !% exceeded
         !         ist2=-2  !stranded onshore
                 ! inbr2=ic3(nel_j,nel)
                 ! nfl=1
                 ! xt=xin
                 ! yt=yin
                 ! zt=zin
                 ! nnel1=nel
                 !exit loop4
         !      endif
            !endif !ic3
         !endif !mod_part

!       Reflect off
!        eps=1.e-2
!        xin=(1-eps)*xin+eps*xctr(nel)
!        yin=(1-eps)*yin+eps*yctr(nel)
         xcg=xin
         ycg=yin
         !Original vel
         uvel0=(xt-xin)/trm
         vvel0=(yt-yin)/trm
         print*,'test refelct off'
         print*,uvel0,xt-xin,xt,xin,trm
         vnorm=uvel0*snx(isd)+vvel0*sny(isd)
         vtan=-uvel0*sny(isd)+vvel0*snx(isd)
         !vtan=-(uu2(md1,jlev0)+uu2(md2,jlev0))/2*sny(isd)+(vv2(md1,jlev0)+vv2(md2,jlev0))/2*snx(isd)
         !Reverse normal vel
         vnorm=-vnorm
         !tmp=max(abs(vtan),1.d-2) !to prevent getting stuck
         !vtan=tmp*sign(1.d0,tmp)
         xvel=vnorm*snx(isd)-vtan*sny(isd)
         yvel=vnorm*sny(isd)+vtan*snx(isd)
         print*,'xvel yvel',yvel,md2,jlev0,md1
         zvel=(ww2(md1,jlev0)+ww2(md2,jlev0))/2
         xt=xin+xvel*trm
         yt=yin+yvel*trm
         zt=zin+zvel*trm
         ! xt = x0
         ! yt = y0
         ! zt = z0      
         print*,'abnormal case',xt,xin,zt,zin
         print*,'it',itt
!        hvel=dsqrt(xvel**2+yvel**2)
!        if(hvel<1.e-4) then
!          write(11,*)'Impossible (5):',hvel
!          nfl=1
!          xt=xin
!          yt=yin
!          zt=zin
!          nnel1=nel
!          exit loop4
!        endif
                                                                !pathl unchanged since hvel is unchanged
!        pathl=hvel*trm
         endif !abnormal cases
!     Search for nel's neighbor with edge nel_j, or in abnormal cases, the same element
         if(lit==0) nel=ic3(nel_j,nel) !next front element
!      aa=0
!      do i=1,3
!        k1=elnode(i,nel)
!        k2=elnode(nxq(1,i,i34(nel)),nel)
!        aa=aa+dabs(signa(x(k1),x(k2),xt,y(k1),y(k2),yt))
!      enddo !i
!      ae=dabs(aa-area(nel))/area(nel)
!      if(ae<small1) then
            call pt_in_poly2(i34(nel),x(elnode(1:i34(nel),nel)),y(elnode(1:i34(nel),nel)),xt,yt,inside)
            if(inside/=0) then
               nnel1=nel
               print*,'exit l4',itt
               exit loop4
            endif

!     Next intersecting edge
            do j=1,i34(nel)
                jd1=elnode(nxq(1,j,i34(nel)),nel)
                jd2=elnode(nxq(2,j,i34(nel)),nel)
!        For abnormal case, same side (border side) cannot be hit again
                if(jd1==md1.and.jd2==md2.or.jd2==md1.and.jd1==md2) cycle
                   call intersect2(xcg,xt,x(jd1),x(jd2),ycg,yt,y(jd1),y(jd2),iflag,xin,yin,tt1,tt2)
                   if(iflag==1) then
                     nel_j=j !next front edge          
                     cycle loop4
                   endif
                  enddo !j
                  print*,'Failed to find next edge I:',lit,xin,yin,xt,yt,nel,md1,md2,idt,rnds
                  stop
!----------------------------------------------------------------------------------------
             end do loop4 

400   continue
!     No vertical exit from domain
      if(idry_e(nnel1)==1) then
         print*,'Ending element is dry'
         stop
      endif

!     Compute area & sigma coord.
                                                        !call area_coord(nnel1,xt,yt,arco)
      call pt_in_poly3(i34(nnel1),x(elnode(1:i34(nnel1),nnel1)),y(elnode(1:i34(nnel1),nnel1)), &
     &xt,yt,arco,nodel2)
      n1=elnode(nodel2(1),nnel1)
      n2=elnode(nodel2(2),nnel1)
      n3=elnode(nodel2(3),nnel1)
      !print*,n1,n2,n3,nnel1,'n1n2n3nnel1'
      etal=eta3(n1)*arco(1)+eta3(n2)*arco(2)+eta3(n3)*arco(3)
      dep=dp(n1)*arco(1)+dp(n2)*arco(2)+dp(n3)*arco(3)
      dp_p=dep
      !print*,'dep',dep,dp(n1),dp(n2),dp(n3)
      if(etal+dep<h0) then
        print*,'Weird wet element in quicksearch:',nnel1,eta3(n1),eta3(n2),eta3(n3),h0
        stop
      endif
!     Compute z-levels
      if(ivcor==2) then
         call zcor_SZ_double(dep,etal,h0,h_s,h_c,theta_b, &
      &theta_f,kz,nvrt,ztot,sigma,zlcl,idry_tmp,kbpl)
         ztmp(kbpl:nvrt)=zlcl(kbpl:nvrt)
      else if(ivcor==1) then
         kbpl=nvrt+1 !local bottom index (maybe degenerate)
         do j=1,3
            nd=elnode(nodel2(j),nnel1)
            if(kbp(nd)<kbpl) kbpl=kbp(nd)
         enddo !j
         !ztmp(kbpl)=-dep 
         !ztmp(nvrt)=etal
         do k=kbpl,nvrt
            ztmp(k)=0
            do j=1,3
               nd=elnode(nodel2(j),nnel1)
               tmp=(eta3(nd)+dp(nd))*sigma_lcl(max(kbp(nd),k),nd)+eta3(nd)
               ztmp(k)=ztmp(k)+tmp*arco(j)
            enddo !j
         enddo !k
         else
           print*,'Unknown ivcor:',ivcor
           stop
         endif
           do k=kbpl+1,nvrt
             if(ztmp(k)-ztmp(k-1)<0) then !can be 0 for deg. case
                print*,'Inverted z-level in quicksearch:',nnel1,etal,dep,k,ztmp(k)-ztmp(k-1),ztmp(kbpl:nvrt)
                stop
             endif
           enddo !!k
           if(zt<=ztmp(kbpl)) then
          !Avoid getting stuck at bottom
              !zt=ztmp(kbpl+1)
              print*,'zt <ztmp',zt,ztmp(kbpl+1),kbpl+1,ztmp(kbpl)
              zt=ztmp(kbpl+1)
              zrat=0
              jlev1=kbpl+1
              print*,'jlev corrected'
           else if(zt>=ztmp(nvrt)) then
              !print*,'else zt',zt,ztmp(nvrt),nvrt,kbpl
              zt=ztmp(nvrt)
              zrat=0
              jlev1=nvrt
           else
              jlev1=0
              do k=kbpl,nvrt-1
                 if(zt>=ztmp(k).and.zt<=ztmp(k+1)) then 
                    jlev1=k+1
                    exit
                 endif
              enddo !k
              if(jlev1==0) then
                 print*,'Cannot find a vert. level:',zt,etal,dep
                 print*,'------'
                 print*,(ztmp(k),k=kbpl,nvrt)
                 stop
              endif
              zrat=(ztmp(jlev1)-zt)/(ztmp(jlev1)-ztmp(jlev1-1))
              !print*,'zrat',zrat
           endif
           print*,'end overflow loop'
           if(zrat<0.or.zrat>1) then
              print*,'Sigma coord. wrong (4):',jlev1,zrat
              stop
           endif
421   continue
!      if(kbpl==kz) then !in pure S region
!        ss=(1-zrat)*sigma(jlev1-kz+1)+zrat*sigma(jlev1-kz)
!      else
!        ss=-99
!      endif
!           return
      end subroutine quicksearch

!======================================================================
      subroutine pt_in_poly2(i34,x,y,xp,yp,inside)
!     (Double-precision) Routine to perform point-in-polygon
!     (triangle/quads) test.
!     Inputs:
!            i34: 3 or 4 (type of elem)
!            x(i34),y(i34): coord. of polygon/elem. (counter-clockwise)
!            xp,yp: point to be tested
!     Outputs:
!            inside: 1, inside
      use globlib, only : small1
      implicit real*8(a-h,o-z)
      integer, intent(in) :: i34
      real*8, intent(in) :: x(i34),y(i34),xp,yp
      integer, intent(out) :: inside
      real*8 :: swild(i34)
 
      inside=0
      do j=1,i34
         j1=j+1
         if(j1>i34) j1=j1-i34
            swild(j)=signa(x(j),x(j1),xp,y(j),y(j1),yp)
      enddo !j
      ae=minval(swild(1:i34))
      if(ae>-small1) inside=1
      end subroutine pt_in_poly2

!======================================================================
      subroutine pt_in_poly3(i34,x,y,xp,yp,arco,nodel)
!     (Double-precision) Routine to perform point-in-polygon
!     (triangle/quads) test with assumption that it's inside, and calculate the area coord.
!     (for quad, split it into 2 triangles and return the 3 nodes and
!     area coord.)
!     Inputs:
!            i34: 3 or 4 (type of elem)
!            i34: 3 or 4 (type of elem)
!            x(i34),y(i34): coord. of polygon/elem. (counter-clockwise)
!            xp,yp: point to be tested
!     Outputs:
!            arco(3), nodel(3) : area coord. and 3 local node indices (valid only if inside)
      implicit real*8(a-h,o-z)
      integer, intent(in) :: i34
      real*8, intent(in) :: x(i34),y(i34),xp,yp
      integer, intent(out) :: nodel(3)
      real*8, intent(out) :: arco(3)
      !Local
      integer :: list(3)
      real*8 :: ar(2),swild(2,3)

      !Areas of up to 2 triangles
      ar(1)=signa(x(1),x(2),x(3),y(1),y(2),y(3))
      ar(2)=0 !init
      if(i34==4) ar(2)=signa(x(1),x(3),x(4),y(1),y(3),y(4))
         if(ar(1)<=0.or.i34==4.and.ar(2)<=0) then
            print*, 'Negative area:',i34,ar,x,y
            stop
         endif
              
         ae_min=huge(1.0d0)
         do m=1,i34-2 !# of triangles
            if(m==1) then
               list(1:3)=(/1,2,3/) !local indices
            else !quads
               list(1:3)=(/1,3,4/)
            endif !m
            aa=0
            do j=1,3
               j1=j+1
               j2=j+2
               if(j1>3) j1=j1-3
                  if(j2>3) j2=j2-3
                    swild(m,j)=signa(x(list(j1)),x(list(j2)),xp,y(list(j1)),y(list(j2)),yp) !temporary storage
                    aa=aa+abs(swild(m,j))
                    !print*,aa
                  enddo !j=1,3

                  ae=abs(aa-ar(m))/ar(m)
                  !print*,'ae',ae
               if(ae<=ae_min) then
                  ae=ae_min
                  nodel(1:3)=list(1:3)
                  arco(1:3)=swild(m,1:3)/ar(m)
                  arco(1)=max(0.d0,min(1.d0,arco(1)))
                  arco(2)=max(0.d0,min(1.d0,arco(2)))
                  if(arco(1)+arco(2)>1) then
                     arco(3)=0
                     arco(2)=1-arco(1)
                  else
                     arco(3)=1-arco(1)-arco(2)
                  endif
                  !print*,'arco',arco
               endif
            enddo !m
      end subroutine pt_in_poly3

      subroutine init_partloc(spwnm,nspawns,nparticles,maxnpar,xlim,ylim,xpar,ypar,zpar)
      implicit none 
      real*8 :: xlim(nspawns,2),ylim(nspawns,2),xparr(nspawns,nparticles),yparr(nspawns,nparticles)
      real*8,intent(inout) :: xpar(maxnpar),ypar(maxnpar)
      real*8,intent(inout) :: zpar(maxnpar)
      integer,intent(in) :: spwnm,nspawns,nparticles,maxnpar
      real*8 :: rand_intx,rand_inty
      real*8 :: rand_val,xlow,ylow,xmax,ymax 
      integer :: i,k,n,j,ind
      real*8 :: zparr(nspawns,nparticles)
      integer,allocatable :: seed(:)
      if(spwnm ==1) then
      xpar(1:nparticles) = xlim(1,1)
      ypar(1:nparticles) = ylim(1,1)
      zpar(1:nparticles) = -17.0
      elseif(spwnm==2) then
      j=1
      call random_seed(size = n)
      allocate(seed(n))
      call random_seed(get=seed)
      if(nspawns == 1) then ;
         xlow = xlim(1,1)
         ylow = ylim(1,1)
         xmax = xlim(1,2)
         ymax = ylim(1,2)
!        print*,'xlow',xlow
         do k=1,nparticles
            j=j+1       
            call random_number(rand_val)
            rand_intx = xlow+floor((xmax+1-xlow)*rand_val)
            xparr(1,k) = rand_intx
            !print*,'xparr',xparr(1,k),rand_val,xlow,ylow
            rand_inty = ylow+floor((ymax+1-ylow)*rand_val)
            yparr(1,k) = rand_inty
            zparr(1,k) = -17.0
         enddo 
         elseif(nspawns .gt. 1) then
            do i=1,nspawns
               xlow = xlim(i,1)
               ylow = ylim(i,1)
               xmax = xlim(i,2)
               ymax = ylim(i,2)
               do k=1,nparticles;
                  call random_number(rand_val)
                  rand_intx = xlow+floor((xmax+1-xlow)*rand_val)
                  xparr(i,k) = rand_intx
!         print*,'xparr',xparr(i,k)
                  rand_inty = ylow+floor((ymax+1-ylow)*rand_val)
                  yparr(i,k) = rand_inty
                  zparr(i,k) = -17.0 !eggs located at the bottom due to stickyness
               enddo 
            enddo
         endif 

! combine xpar into a single vector with max size X
         if(nspawns == 1) then 
            xpar(1:nparticles)=xparr(1,1:nparticles)
            !print*,xparr(1,1)
            ypar(1:nparticles)=yparr(1,1:nparticles)
            zpar(1:nparticles)=zparr(1,1:nparticles)
         elseif(nspawns.gt.1) then
            do i=1,nspawns 
               xpar(1:nparticles) = xparr(i,1:nparticles)
               ypar(1:nparticles) = yparr(i,1:nparticles)
               zpar(1:nparticles) = zparr(i,1:nparticles)
               if(i.gt.1) then 
                  ind = (i-1)+nparticles
                  xpar(ind:(ind-1)+nparticles) = xparr(i,1:nparticles)
                  ypar(ind:(ind-1)+nparticles) = yparr(i,1:nparticles)
                  zpar(ind:(ind-1)+nparticles) = zparr(i,1:nparticles)
               endif
            enddo 
         endif
        endif
         !print*,'xpar(1),xpar(200)',xpar(1),xpar(100),xpar(101),xpar(200)
         !print*,'ypar(1),ypar(200)',ypar(1),ypar(100),ypar(101),ypar(200) 
         end subroutine init_partloc

         subroutine partstat_check_vector(maxnpar,parstat,var,nnes)
         implicit none 
         integer,intent(inout) :: maxnpar,parstat(maxnpar)
         integer,intent(out) :: nnes
         real*8,intent(inout) :: var(maxnpar)
         integer :: i
         real*8 :: var2(maxnpar)
         nnes = 0
         var2=0.0
         !for-loop over all entries in the matrix 
         do i=1,maxnpar
            if (parstat(i)==0) then 
               var2(i)=0.0
            elseif(parstat(i)>=1) then 
               var2(i)=var(i) 
            endif  
         end do 
         !filling the other values with zeroes 
         var=var2
         return
         end subroutine partstat_check_vector

         subroutine partstat_check_int(maxnpar,parstat,var1,var2,nnes)
         implicit none 
         integer,intent(inout) :: maxnpar,parstat(maxnpar)
         integer,intent(out) :: nnes
         integer,intent(inout) :: var1(maxnpar),var2(maxnpar)
         integer :: i,j
         integer :: vvar1(maxnpar),vvar2(maxnpar)

         nnes = 0
         vvar1 = 0
         vvar2 = 0

         j=1
         do i=1,maxnpar
         ! check for dead particles (istage==4)
            if(var1(i)==4) then  
                parstat(i)=0
                print*,'particle died'
            endif 
            if(parstat(i)>=1) then
                vvar1(i) = var1(i)
                vvar2(i) = var2(i)
            elseif(parstat(i)==0) then 
                vvar1(i)=4
                vvar2(i)=0
            endif
         enddo 
         var1 = vvar1
         var2 = vvar2
         return
         end subroutine partstat_check_int

         subroutine partstat_check_vector_writeout(time,maxnpar,parstat,var,iep,nnes)
         implicit none 
         integer,intent(inout) :: maxnpar,parstat(maxnpar),iep(maxnpar) 
         integer,intent(out) :: nnes
         integer,intent(inout) :: var(maxnpar)
         integer:: i
         integer :: var2(maxnpar),iep2(maxnpar)
         real*8,intent(in) :: time
         nnes = 0
         var2=0.0
         !for-loop over all entries in the matrix 
         do i=1,maxnpar
            if(parstat(i)>=1) then 
               var2(i) = var(i)
               iep2(i) = iep(i)
            elseif(parstat(i)==0) then
               iep2(i)=0
               var2(i)=var(i)  
            endif
         enddo
         !loop to reset parstat to the right values based on the index
         !being an index or a zero 
         var = var2
         iep=iep2
         end subroutine partstat_check_vector_writeout

         subroutine particle_respawn(nspawns,parstat,nparticles,maxnpar,parind,spwnnum,nnes,xlim,ylim,xpar,ypar,zpar)
         use globlib
         implicit none
         integer,intent(in)::nnes,nspawns,nparticles,maxnpar
         real*8 :: xlim(nspawns,2),ylim(nspawns,2),xparr(nspawns,nparticles),yparr(nspawns,nparticles)
         real*8:: xxpar(maxnpar),yypar(maxnpar),zzpar(maxnpar)
         real*8,intent(inout) :: xpar(maxnpar),ypar(maxnpar)
         real*8,intent(inout) :: zpar(maxnpar)
         real*8 :: rand_intx,rand_inty
         real*8 :: rand_val,xlow,ylow,xmax,ymax 
         integer,intent(inout) :: parstat(maxnpar)
         integer,intent(inout) :: parind
         integer :: i,j,n,k,ind,spwnnum,pardiff,parind2,inside
         real*8 :: zparr(nspawns,nparticles)
         integer,allocatable :: seed(:)
         integer,allocatable :: ll(:)
         j=1
         call random_seed(size = n)
         allocate(seed(n))
         call random_seed(get=seed)
         parind=0
         parind2=0
         do i=1,maxnpar
            if(parstat(i)==1) then 
               parind=parind+1
            endif  
         enddo
         print*,'parind in respawn',parind
         if (parind==maxnpar) goto 200
         ! calculate the difference between parind and maxnpar 
            pardiff = maxnpar-parind
            if(nspawns == 1) then ;
               xlow = xlim(1,1)
               ylow = ylim(1,1)
               xmax = xlim(1,2)
               ymax = ylim(1,2)
               !print*,'ylim,xlim',xlim,ylim
               if(pardiff.ge.nparticles) then 
                  spwnnum=nparticles
                  elseif(pardiff.lt.nparticles) then 
                     spwnnum=pardiff
                  endif
                  print*,spwnnum,'spwnnum','pardiff',pardiff 
                  do k=1,spwnnum
                     j=j+1       
                     call random_number(rand_val)
                     !  print*,'randval',rand_val,xlow,xmax
                     rand_intx = xlow+floor((xmax+1-xlow)*rand_val)
                     xparr(1,k) = rand_intx
                     ! print*,'rand_itx',rand_intx,xparr(1,k)
                     rand_inty = ylow+floor((ymax+1-ylow)*rand_val)
                     yparr(1,k) = rand_inty
                     zparr(1,k) = -1.0
                  enddo 
                  elseif(nspawns .gt. 1) then
                     do i=1,nspawns
                        xlow = xlim(i,1)
                        ylow = ylim(i,1)
                        xmax = xlim(i,2)
                        ymax = ylim(i,2)
                        if(pardiff.ge.nparticles) then 
                           spwnnum=nparticles
                        elseif(pardiff.lt.nparticles) then 
                           spwnnum=pardiff
                        endif 
                        do k=1,spwnnum;
                           call random_number(rand_val)
                           rand_intx = xlow+floor((xmax+1-xlow)*rand_val)
                           xparr(i,k) = rand_intx
                           rand_inty = ylow+floor((ymax+1-ylow)*rand_val)
                           yparr(i,k) = rand_inty
                           zparr(i,k) = -1.0 !eggs located at the bottom due to stickyness
                        enddo 
                     enddo
                  endif 
                 !moving all the new particles in a new vector 
                  if(nspawns==1) then 
                     xxpar(1:nparticles) = xparr(1,1:nparticles)
                     !  print*,xxpar(1),'xxpar'
                     yypar(1:nparticles) = yparr(1,1:nparticles) 
                     zzpar(1:nparticles) = zparr(1,1:nparticles) 
                  elseif(nspawns.gt.1) then  
                     do i=1,nspawns
                        xxpar(1:nparticles) = xparr(i,1:nparticles)
                        yypar(1:nparticles) = yparr(i,1:nparticles)
                        zzpar(1:nparticles) = zparr(i,1:nparticles)
                        if(i.gt.1) then 
                           ind=(i-1)+nparticles
                           xxpar(ind:(ind-1)+nparticles) = xparr(i,1:nparticles)
                           yypar(ind:(ind-1)+nparticles) = yparr(i,1:nparticles)
                           zzpar(ind:(ind-1)+nparticles) = zparr(i,1:nparticles) 
                        endif 
                     enddo 
                  endif 
!       print*,'testing for initial elements'
                  !lp2: do i=1,maxnpar
                  ! do k=1,ne
                  ! call pt_in_poly2(i34(k),x(elnode(1:i34(k),k)),y(elnode(1:i34(k),k)),xxpar(i),yypar(i),inside)
!         print*,'xpar(2)',xpar(i),ypar(i),inside
                  !if(inside/=0) then 
                  !   ielpar(parind+(i-1)) = k
                  !   cycle lp2
                  !endif
                  !enddo 
                  !write(11,*)'Cannot find init. element for particle',i
                  !parstat(parind+(i-1)) = -1 !particle is dead
                  !end do lp2 !i=1,nparticles 
                  
                  k=1
                  print*,'parind',parind  
                  do j=parind+1,parind+spwnnum
                     xpar(j) = xxpar(k)
                     ! print*,'xpar(j),xxpar(k)',xpar(j),xxpar(k)
                     ypar(j) = yypar(k)
                     zpar(j) = zzpar(k)
                     k=k+1
                     print*,'j and k',j,k
                     print*,'xp',xpar(j),'xxp',xxpar(k)
                  enddo    
                  ! xpar(parind:parind+spwnnum) = xxpar(1:spwnnum)
                  ! print*,'xpar(parind)',xpar(parind+1)
                  !ypar(parind:parind+spwnnum) = yypar(1:spwnnum) 
                  ! zpar(parind:parind+spwnnum) = zzpar(1:spwnnum)
                  ! print*,'parind and id(parind)',parind,idpar(parind),xpar(parind+2)
                  !combining into xpar,ypar and zpari
200     continue         
                  return
                  end subroutine particle_respawn
      subroutine netcdf_output(Idp,Xp,Yp,Zp,Tp,Sp,Up,Vp,Wp,gx,gy,dhx,Mizop,Mezop,turbp, &
      & Oxyp,Prep,dryc,Stp,Nind,extt,iepa,levp,Elep,Kbpp,itot,iit,maxnpar,otime, &
      & iihot,ihot_start)
      use globlib 
      use ibm
      use netcdf 
      implicit none
      integer,intent(in) :: maxnpar,iihot,ihot_start 
      real*8,intent(in)::Xp(maxnpar),Yp(maxnpar),Zp(maxnpar),Tp(maxnpar),Sp(maxnpar)
      real*8,intent(in)::Up(maxnpar),Vp(maxnpar),Wp(maxnpar),Oxyp(maxnpar)
      real*8,intent(in)::Mizop(maxnpar),Mezop(maxnpar),Prep(maxnpar)
      real*8,intent(in)::Nind(maxnpar),extt(maxnpar),dryc(maxnpar)
      real*8,intent(in)::Elep(maxnpar),Kbpp(maxnpar),turbp(maxnpar)
      real*8,intent(in) :: gx(maxnpar),gy(maxnpar),dhx(maxnpar)
      integer,intent(in):: iepa(maxnpar),levp(maxnpar)
      real*8,intent(in) :: otime
      integer,intent(in) :: Idp(maxnpar),Stp(maxnpar),itot,iit
      integer :: fileID,iret,start(2),count(2),dday(1),idiffi
      integer :: start_1d(1),count_1d(1)
      integer :: vX,vY,vZ,vT,vS,vU,vV,vW,vST,vTime,vI,vN,vE,vPr,vTu
      integer ::vFla,vDia,vMizo,vMezo,vDetri,vNH4,vNO3,vPO4,vSil,vOxy,vEx
      integer :: vLev,vIep,vElep,vKbp,vDc,vGX,vGY,vDHX
      real*8::ttime(1),atime(1)
      !open the nc-output file earlier defined in the main program 
      idiffi = iit+1-itot
      print*,'idiffi calc',iit,itot
      !open(104,file='output_debug.txt',access='append')
      !write(104,*),'in nc_output routine'
      !write(104,*),'xpar',Xp
      !write(104,*),'zpar',Zp
      !write(104,*),'upar',Up
      !write(104,*),'tpar',Tp
      !close(104)
      !print*,'maxnpar',maxnpar
      if(mod(idiffi,1)==0 .or. idiffi==1) then 
      iret=nf90_open(path='./ibm_output.nc',mode=nf90_write,ncid=fileID)
      if(iret/=nf90_NoErr) stop 'opening of file not possible'
         !print*,'opening worked'
           !aquire the variablesi
           iret=nf90_inq_varid(fileID,'time',vTime)
           if(iret/=nf90_noerr) stop 'time id not found'
           iret=nf90_inq_varid(fileID,'idPar',vI)
           if(iret/=nf90_noerr) stop 'idpar ID not found'
           iret=nf90_inq_varid(fileID,'N_ind',vN)
           if(iret/=nf90_noerr) stop 'n_indi not found'
           iret=nf90_inq_varid(fileID,'xpar',vX)
           if(iret/=nf90_noerr) stop 'xpar ID not found'
           iret=nf90_inq_varid(fileID,'ypar',vY)
           if(iret/=nf90_noerr) stop 'ypar ID not found'
           iret=nf90_inq_varid(fileID,'zpar',vZ)
           if(iret/=nf90_noerr) stop 'zpar id not found'
           iret=nf90_inq_varid(fileID,'tpar',vT)
           if(iret/=nf90_noerr) stop 'temp id not found'
           iret=nf90_inq_varid(fileID,'spar',vS)
           if(iret/=nf90_noerr) stop 'spar id not found'
           iret=nf90_inq_varid(fileID,'upar',vU)
           if(iret/=nf90_noerr) stop 'upar id not found'
           iret=nf90_inq_varid(fileID,'vpar',vV)
           if(iret/=nf90_noerr) stop 'vpar if not found'
           iret=nf90_inq_varid(fileID,'wpar',vW)
           if(iret/=nf90_noerr) stop 'wpar id not found'
           iret=nf90_inq_varid(fileID,'pstage',vST)
           if(iret/=nf90_noerr) stop 'istage if not found'
           iret=nf90_inq_varid(fileID,'mizopar',vMizo)
           if(iret/=nf90_noerr) stop 'mizopar id not found'
           iret=nf90_inq_varid(fileID,'mezopar',vMezo)
           if(iret/=nf90_noerr) stop 'mezopar id not found' 
           iret=nf90_inq_varid(fileID,'oxypar',vOxy)
           if(iret/=nf90_noerr) stop 'oxypar not found'
           iret=nf90_inq_varid(fileID,'extpar',vEx)
           if(iret/=nf90_noerr) stop 'extpar not inq'
           iret=nf90_inq_varid(fileID,'ielpar',vIep)
           if(iret/=nf90_noerr) stop 'ielpar not inq'
           iret=nf90_inq_varid(fileID,'levpar',vLev)
           if(iret/=nf90_noerr) stop 'levpar not inq'
           iret=nf90_inq_varid(fileID,'etap',vElep)
           if(iret/=nf90_noerr) stop 'etaap not found'
           iret=nf90_inq_varid(fileID,'kbpp',vKbp)
           if(iret/=nf90_noerr) stop 'kb not found'
           iret=nf90_inq_varid(fileID,'preypar',vPr)
           if(iret/=nf90_noerr) stop 'preyp not found'
           iret=nf90_inq_varid(fileID,'turbpar',vTu)
           if(iret/=nf90_noerr) stop 'turbpar not found'
           iret=nf90_inq_varid(fileID,'drycount',vDc)
           if(iret/=nf90_noerr) stop 'dryocunt not found'
           iret=nf90_inq_varid(fileID,'grdx',vGX)
           if(iret/=nf90_noerr) stop 'grdx not found'
           iret=nf90_inq_varid(fileID,'grdy',vGY)
           if(iret/=nf90_noerr) stop 'grdy not found'
           iret=nf90_inq_varid(fileID,'dhx',vDHX)
           if(iret/=nf90_noerr) stop 'dhx not found'
           if (idiffi==1) then 
              if (iihot==0) then 
              dday(1)=idiffi
              print*,'no iihot',iihot
              elseif(iihot==1) then 
              dday(1)= idiffi+ihot_start
              print*,'iihot',iihot,ihot_start
              endif 
           else 
              dday(1)=dday(1)+1
           endif
           print*,'idiffi',idiffi

           start(2) = dday(1); start(1)=1
           start_1d(1) = dday(1);count_1d(1)=1
           count(1)=maxnpar; count(2)=1
           atime(1) = otime
           print*,'start nc write',start(:)
           print*,'count nc',count(:)
           !add the new data to these variables 
           iret=nf90_put_var(fileID,vTime,atime,start_1d,count_1d)
           if(iret/=nf90_noerr) stop 'time not written'
           iret = nf90_put_var(fileID,vX,Xp,start,count)
           if(iret/=nf90_noerr) stop 'xpar not written'
           iret = nf90_put_var(fileID,vY,Yp,start,count)
           if(iret/=nf90_noerr) stop 'ypar not written'
           iret = nf90_put_var(fileID,vN,Nind,start,count)
           if(iret/=nf90_noerr) stop 'nindi not written'
           iret = nf90_put_var(fileID,vZ,Zp,start,count)
           if(iret/=nf90_noerr) stop 'zpar not written'
           iret = nf90_put_var(fileID,vT,Tp,start,count)
           if(iret/=nf90_noerr) stop 'tpar not written'
           iret = nf90_put_var(fileID,vS,Sp,start,count)
           if(iret/=nf90_noerr) stop 'spar not written'
           iret = nf90_put_var(fileID,vU,Up,start,count)
           if(iret/=nf90_noerr) stop 'upar not written'
           iret = nf90_put_var(fileID,vV,Vp,start,count)
           if(iret/=nf90_noerr) stop 'vpar not written'
           iret = nf90_put_var(fileID,vW,Wp,start,count) 
           if(iret/=nf90_noerr) stop 'wpar not written'
           iret = nf90_put_var(fileID,vI,Idp,start,count)
           if(iret/=nf90_noerr) stop 'idPar not written'
           iret = nf90_put_var(fileID,vST,Stp,start,count)
           if(iret/=nf90_noerr) stop 'istage not writte'
           iret=nf90_put_var(fileID,vMizo,Mizop,start,count)
           if(iret/=nf90_noerr) stop 'mizo not written'
           iret=nf90_put_var(fileID,vMezo,Mezop,start,count)
           if(iret/=nf90_noerr) stop 'mezo not written'
           iret=nf90_put_var(fileID,vOxy,Oxyp,start,count)
           if(iret/=nf90_noerr) stop 'oxy not written'
           iret=nf90_put_var(fileID,vEx,extt,start,count)
           if(iret/=nf90_noerr) stop 'expar not written'
           iret=nf90_put_var(fileID,vIep,iepa,start,count)
           if(iret/=nf90_noerr) stop 'iepar not written'
           iret=nf90_put_var(fileID,vLev,levp,start,count)
           if(iret/=nf90_noerr) stop 'levpar not written'
           iret=nf90_put_var(fileID,vElep,Elep,start,count)
           if(iret/=nf90_noerr) stop 'etap not written'
           iret=nf90_put_var(fileID,vKbp,Kbpp,start,count)
           if(iret/=nf90_noerr) stop 'Kbp not written'
           iret=nf90_put_var(fileID,vPr,Prep,start,count)
           if(iret/=nf90_noerr) stop 'preyp not written'
           iret=nf90_put_var(fileID,vTu,turbp,start,count)
           if(iret/=nf90_noerr) stop 'turbpar not written'
           iret=nf90_put_var(fileID,vDc,dryc,start,count)
           if(iret/=nf90_noerr) stop 'drycount not written'
           iret=nf90_put_var(fileID,vGX,gx,start,count)
           if(iret/=nf90_noerr) stop 'GX not written'
           iret=nf90_put_var(fileID,vGY,gy,start,count)
           if(iret/=nf90_noerr) stop 'GY not written'
           iret=nf90_put_var(fileID,vDHX,dhx,start,count)
           if(iret/=nf90_noerr) stop 'DHX not written'
           iret=nf90_close(fileID)
           if(iret/=nf90_noerr) stop 'file not closed'
        endif
        return 
        end subroutine netcdf_output
        
        subroutine init_idpar(maxnpar,nparticle,idpar)
        implicit none 
        integer,intent(in) :: nparticle,maxnpar
        integer,intent(inout) :: idpar(maxnpar)
        integer :: i   
        idpar = 0     
        do i=1,nparticle
           idpar(i) = i
        enddo
        return 
        end subroutine init_idpar 
        
        subroutine manage_idpar(maxnpar,xpar,parstat,mid,idpar)
        implicit none 
        integer,intent(in) :: maxnpar,mid
        integer,intent(in) :: parstat(maxnpar)
        integer,intent(inout) :: idpar(maxnpar) 
        real*8,intent(in) :: xpar(maxnpar) 
        integer :: i,j 
        integer :: maxid 
        j=1
        maxid = mid
        print*,'maxid=', maxid
        if(idpar(1) == 0) then 
           do i=1,maxnpar
              if (xpar(i)>0.0) then 
          !       print*,xpar(i)
                 idpar(j) =maxid+j
                 j=j+1
              endif 
           enddo 
           print*,'maxid idpar neu',maxid
        elseif(idpar(1).gt.0) then
           do i=1,maxnpar
              if(xpar(i).gt.0.0 .and. idpar(i) ==0) then 
              idpar(i) = maxid+j
              j=j+1
              endif 
           enddo 
         endif
        print*,idpar
        return
        end subroutine manage_idpar

        subroutine read_biom(ifilee,irece,npar,flage,diat,mizoo,mezoo,detri,dom,nh4,no3,po4,sil,oxy)
        use globlib 
        use netcdf 
        use ibm
        implicit none
        integer,intent(in) :: npar,ifilee,irece
        real(kind=dbl_kind),intent(inout)::flage(np,nvrt),diat(np,nvrt),mizoo(np,nvrt),mezoo(np,nvrt)   
        real(kind=dbl_kind),intent(inout)::detri(np,nvrt),nh4(np,nvrt),no3(np,nvrt),po4(np,nvrt),sil(np,nvrt)
        real(kind=dbl_kind),intent(inout):: oxy(np,nvrt),dom(np,nvrt)
        integer :: len_chare,irete,ncide
        integer :: vFlag,vDia,vmizo,vmezo,vDetri,vNH4
        integer :: vNO3, vPO4, vSil,vOxy,vDom
        real(kind=dbl_kind):: real_er(nvrt,np),test(np,nvrt)
        character(len=40) :: file63e
        character(len=12) :: ifile_chare
        ! Aim of the porgramm is to read the input of the ecosystem
        ! model ecosmo 
        ! allocate(flage(nvrt,np),diat(nvrt,np),mizoo(nvrt,np),mezoo(nvrt,np),detri(nvrt,np),&
        !         &nh4(nvrt,np),no3(nvrt,np),po4(nvrt,np),sil(nvrt,np))
        print*,'read biom'
        write(ifile_chare,'(i12)') ifilee
        ifile_chare=adjustl(ifile_chare);len_chare=len_trim(ifile_chare)
        file63e='./schout_'//ifile_chare(1:len_chare)//'.nc'
        print*,'file63e',file63e
        print*,ifilee,irece,npar
        irete=nf90_open(trim(adjustl(file63e)),OR(NF90_NETCDF4,NF90_NOWRITE),ncide)
        if(irete/=nf90_NoErr) stop 'could not open eco infos'
        !open the variables 
        irete=nf90_inq_varid(ncide,'ECO_fla',vFlag)
        print*,ncide,vFlag
        if(irete/=nf90_noerr) stop 'flag not inq'
        irete=nf90_inq_varid(ncide,'ECO_dia',vDia)
        if(irete/=nf90_noerr) stop 'dia not inq'
        irete=nf90_inq_varid(ncide,'ECO_microzoo',vmizo)
        if(irete/=nf90_noerr) stop 'microzoo not inq'
        irete=nf90_inq_varid(ncide,'ECO_mesozoo',vmezo)
        if(irete/=nf90_noerr) stop 'mesozoo not inq'
        irete=nf90_inq_varid(ncide,'ECO_det',vDetri)
        if(irete/=nf90_noerr) stop 'detrius no inq'
        irete=nf90_inq_varid(ncide,'ECO_nh4',vNH4)
        if(irete/=nf90_noerr) stop 'nh4 not inq'
        irete=nf90_inq_varid(ncide,'ECO_no3',vNO3)
        if(irete/=nf90_noerr) stop 'no3 not inq'
        irete=nf90_inq_varid(ncide,'ECO_pho',vPO4)
        if(irete/=nf90_noerr) stop 'po4 not inq'
        irete=nf90_inq_varid(ncide,'ECO_sil',vSil)
        if(irete/=nf90_noerr) stop 'sil not inq'
        irete=nf90_inq_varid(ncide,'ECO_oxy',vOxy)
        if(irete/=nf90_noerr) stop 'oxy not inq'
        irete=nf90_inq_varid(ncide,'ECO_dom',vDom)
        if(irete/=nf90_noerr) stop 'dom not inq'
        ! inq of var done now get the values 
        irete=nf90_get_var(ncide,vFlag,real_er(1:nvrt,1:np),(/1,1,irece/),(/nvrt,np,1/))
        if(irete/=nf90_noerr) print*,trim(nf90_strerror(irete))
        flage=transpose(real_er)
        
        irete=nf90_get_var(ncide,vDia,real_er(1:nvrt,1:np),(/1,1,irece/),(/nvrt,np,1/))
        if(irete/=nf90_noerr) stop 'could not read diatrus'
        diat = transpose(real_er(1:nvrt,1:np))
        
        irete=nf90_get_var(ncide,vmizo,real_er(1:nvrt,1:np),(/1,1,irece/),(/nvrt,np,1/))
        if(irete/=nf90_noerr) stop 'could not read mizoo'
        mizoo(:,:) = transpose(real_er(1:nvrt,1:np))
        
        irete=nf90_get_var(ncide,vmezo,real_er(1:nvrt,1:np),(/1,1,irece/),(/nvrt,np,1/))
        if(irete/=nf90_noerr) stop 'could not read mezoo'
        mezoo(:,:) = transpose(real_er(1:nvrt,1:np))

        irete=nf90_get_var(ncide,vDetri,real_er(1:nvrt,1:np),(/1,1,irece/),(/nvrt,np,1/))
        if(irete/=nf90_noerr) stop 'could not read detri'
        detri(:,:) = transpose(real_er(1:nvrt,1:np))

        irete=nf90_get_var(ncide,vNH4,real_er(1:nvrt,1:np),(/1,1,irece/),(/nvrt,np,1/))
        if(irete/=nf90_noerr) stop 'could not read nh4'
        nh4(:,:) = transpose(real_er(1:nvrt,1:np))

        irete=nf90_get_var(ncide,vNO3,real_er(1:nvrt,1:np),(/1,1,irece/),(/nvrt,np,1/))
        if(irete/=nf90_noerr) stop 'could not read no3'
        no3(:,:) = transpose(real_er(1:nvrt,1:np))
        
        irete=nf90_get_var(ncide,vPO4,real_er(1:nvrt,1:np),(/1,1,irece/),(/nvrt,np,1/))
        if(irete/=nf90_noerr) stop 'could not read po4'
        po4(:,:) = transpose(real_er(1:nvrt,1:np))
        
        irete=nf90_get_var(ncide,vSil,real_er(1:nvrt,1:np),(/1,1,irece/),(/nvrt,np,1/))
        if(irete/=nf90_noerr) stop 'could not read Siliicate'
        sil(:,:) = transpose(real_er(1:nvrt,1:np))

        irete=nf90_get_var(ncide,vOxy,real_er(1:nvrt,1:np),(/1,1,irece/),(/nvrt,np,1/))
        if(irete/=nf90_noerr) stop 'could not read oxy'
        oxy(:,:) = transpose(real_er(1:nvrt,1:np))

        irete=nf90_get_var(ncide,vDom,real_er(1:nvrt,1:np),(/1,1,irece/),(/nvrt,np,1/))
        if(irete/=nf90_noerr) stop 'could not read dom'
        dom(:,:) = transpose(real_er(1:nvrt,1:np))
        irete=nf90_close(ncide)
        return
        end subroutine read_biom

        subroutine conv_biom(flag,diat,fpr,plx,wpr,prey,iipr,prmin,pincr)
        ! conversion of the biomass of fla and diat according to 
        ! Daewel 2008 
        use globlib
        use ibm
        implicit none
        integer,intent(in) :: iipr,prmin,pincr
        real*8,intent(in) :: flag(np,nvrt),diat(np,nvrt)
        real*8,intent(inout) ::fpr(iipr),wpr(iipr),plx(iipr)
        real*8,intent(out) ::prey(np,nvrt)
        integer :: jj,kk
        real*8 :: totbiom,plstart
        real*8, allocatable :: fun(:),pll(:),conc(:)

        plstart = real(prmin)
        !print*,'plstart',plstart
        ! defining of some parameters (again following Daewel 2008 
        ! changes for the elbe setup might be necessary
!        print*,'debug conv 0'
        allocate(fun(iipr),pll(iipr),conc(iipr))
!        print*,'debug00'
        prey = 0.0
        do jj=1,np
           do kk =1,nvrt
              prey(jj,kk) = (flag(jj,kk)+diat(jj,kk))*10.**(-6.)
           enddo 
        enddo
        print*,'preycalc check'
        print*,minval(prey),maxval(prey)
        do jj = 1,iipr
        fun(jj) = 0.0
        pll(jj) = 0.0
        conc(jj) = 0.0
        enddo 
        plx(1) = plstart*10.0**(-3.0)
        pll(1) = plstart
        !print*,pll(1),'pll(1)'
!        print*,plx(1),pll(1)
        do jj = 2,iipr
           pll(jj) = pll(jj-1)+real(pincr)
           plx(jj) = plx(jj-1)+real(pincr)*10.0**(-3.0) !length [mm]
        enddo
        do jj = 1,iipr
        wpr(jj) = 10.0**(2.7722*log10(pll(jj))-7.47607)
        conc(jj) = 695.73*exp(-0.0083*pll(jj))
        fun(jj) =(conc(jj)*wpr(jj))
        totbiom = totbiom + fun(jj)
        enddo
        do jj= 1,iipr
        fun(jj)=fun(jj)/totbiom
        fpr(jj) = fun(jj)/wpr(jj)
        enddo 
       ! print*,'fpr'
        !print*,fpr
        return

        end subroutine conv_biom

        subroutine read_hotstart_position(nparticles,tthot,ifile_hot,xp,yp,zp,lep_j,iep_j,nphot,idphot,st_phot,thshot,iihot,ihstart)
        use netcdf
        use kind_par
        implicit none
        integer,intent(inout)::nparticles,ifile_hot,iihot,lep_j(nparticles)
        real*8,intent(inout)::xp(nparticles),yp(nparticles),zp(nparticles),nphot(nparticles)
        integer,intent(inout)::idphot(nparticles),thshot
        integer,intent(inout)::ihstart
        real*8,intent(inout)::st_phot(nparticles),tthot(1)    
        integer ::nchot,irhot,ihst,lep_hot(nparticles),iep_j(nparticles)
        real(kind=dbl_kind1)::thot(1),xhot(nparticles),yhot(nparticles),zhot(nparticles),nhot(nparticles)
        integer ::idhot(nparticles),ithshot(1),psthot(nparticles),ielph(nparticles)
        real*8::stphot(nparticles)
        integer :: i1,i2,i3,i4,i5,i6,i7,i8,i9,i10,i11,i12
        real*8:: real_er(nparticles),real_ar(1)
        irhot=nf90_open(path='./hotstart_ptrack.nc',mode=nf90_nowrite,ncid=nchot)
        if(irhot/=nf90_noerr) stop 'hotstart_ptrack.nc not opened' 
        print*,iihot
        irhot = nf90_inq_varid(nchot,'time1',i1)
        if(irhot/=nf90_noerr) stop 'could not inq time'
        irhot = nf90_inq_varid(nchot,'xpar1',i2)
        if(irhot/=nf90_noerr) stop 'could not inq xpar'
        irhot = nf90_inq_varid(nchot,'ypar1',i3)
        if(irhot/=nf90_noerr) stop 'could not inq ypar'
        irhot = nf90_inq_varid(nchot,'zpar1',i4)
        if(irhot/=nf90_noerr) stop 'could not inq zpar'
        irhot = nf90_inq_varid(nchot,'npar',i5)
        if(irhot/=nf90_noerr) stop 'could not inq npar'
        irhot = nf90_inq_varid(nchot,'idpar',i6)
        if(irhot/=nf90_noerr) stop 'could not inq idpar'
        irhot = nf90_inq_varid(nchot,'iths1',i7)
        if(irhot/=nf90_noerr) stop 'could not inq iths'
        print*,'inq done'
        irhot = nf90_inq_varid(nchot,'st_p',i9)
        if(irhot/=nf90_noerr) stop 'could not inq st_p'
        irhot=nf90_inq_varid(nchot,'hot_start',i10)
        if(irhot/=nf90_noerr) stop 'could not inq ih_start'
        irhot = nf90_inq_varid(nchot,'levp_hot',i11)
        if(irhot/=nf90_noerr) stop 'could not inq levpar'
        irhot=nf90_inq_varid(nchot,'ielp_hot',i12)
        if(irhot/=nf90_noerr) stop 'could not inq ielp'
        !reading the variables 
        
        irhot=nf90_get_var(nchot,i1,real_ar(1))
        if(irhot/=nf90_noerr) stop 'could not read time'
        thot = real_ar(1)
        !print*,thot
        
        irhot = nf90_get_var(nchot,i2,real_er(1:nparticles))
        if(irhot/=nf90_noerr) stop 'could not read xpar'
        xhot = real_er(:)
        !print*,xhot(1)
        
        irhot = nf90_get_var(nchot,i3,real_er(1:nparticles))
        if(irhot/=nf90_noerr) stop 'could not read ypar'
        yhot = real_er(:)
        !print*,yhot(1)
        
        irhot = nf90_get_var(nchot,i4,real_er(1:nparticles))
        if(irhot/=nf90_noerr) stop 'could not read zpar'
        zhot = real_er(:)
        !print*,zhot(1)
        
        irhot = nf90_get_var(nchot,i5,real_er(1:nparticles))
        if(irhot/=nf90_noerr) stop 'could not read npar'
        nhot = real_er(:)
        !print*,nhot(1)
        
        irhot = nf90_get_var(nchot,i6,real_er(1:nparticles))
        if(irhot/=nf90_noerr) stop 'could not read Idpar'
        idhot = real_er(:)
        !print*,idhot(1)
        
        irhot = nf90_get_var(nchot,i7,real_ar(1))
        if(irhot/=nf90_noerr) stop 'could not read iths'
        ithshot = real_ar(1)
        print*,'ithshot',ithshot 
        irhot = nf90_get_var(nchot,i9,real_er(1:nparticles))
        if(irhot/=nf90_noerr) stop 'could not read st_p'
        stphot = real_er(:)
        
        irhot = nf90_get_var(nchot,i10,real_ar(1))
        if(irhot/=nf90_noerr) stop 'could not read ihot_s'
        print*,'real_ar',real_ar(1)
        ihst = int(real_ar(1))
        irhot=nf90_get_var(nchot,i11,real_er(1:nparticles))
        if(irhot/=nf90_noerr) stop 'could no read levpar_hot'
        lep_hot = real_er(:)

        irhot=nf90_get_var(nchot,i12,real_er(1:nparticles))
        if(irhot/=nf90_noerr) stop 'could not read ielp'
        ielph = real_er(:)
        irhot=nf90_close(nchot)
        print*,'ihstart',ihst
        ! einbauen der output variablen
        xp(:) = xhot(:)
        !print*,'xp',xp(1)
        print*,xp(1:20)
        yp(:) = yhot(:)
        lep_j(:) = int(lep_hot)
        print*,'lep_j',lep_j(1:10)
        iep_j(:) = int(ielph)
        zp(:) = zhot(:)
        nphot(:) = nhot(:)
        !print*,'nphot',nphot(1)
        idphot(:) = idhot(:)
        !print*,'idphot',idphot(1)
        print*,'ithshot',ithshot(1)
        tthot(1) = real(thot(1)*86400.0)
        ifile_hot = int(thot(1))+1
        !print*,ifile_hot
        print*,'tthot',tthot(1)
        thshot = ithshot(1)
        st_phot(:) = stphot(:)
        print*,'st_phot',stphot(1)
        ihstart = ihst
        return 
        end subroutine read_hotstart_position
!======================================================================
